<?php
    session_start();
    if (!isset($_SESSION['email']))
    {
        echo "
                <script type='text/javascript'>
                    window.alert('Bạn không được phép truy cập');
                    window.location.href='dang_nhap.php';
                </script>
             ";
    }
;?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>Quản lý chi nhánh</title>
  <!-- Favicon -->
  <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">
</head>

<body>
  <!-- Sidenav -->
  <nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <div class="sidenav-header  align-items-center">
        <a class="navbar-brand" href="../trangchu.php">
          <img src="../assets/img/brand/abca.png" class="navbar-brand-img" alt="...">
        </a>
      </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="../index.php">
                <i class="ni ni-tv-2 text-primary"></i>
                <span class="nav-link-text">Quản lý hệ thống</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="nguoi_dung.php">
                <i class="fas fa-angle-right"></i>
                <span class="nav-link-text">Quản lý người dùng</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="tin_tuc.php">
                <i class="fas fa-angle-right"></i>
                <span class="nav-link-text">Quản lý tin tức</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="san_pham.php">
                <i class="fas fa-angle-right"></i>
                <span class="nav-link-text">Quản lý sản phẩm</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="dat_hang_ex.php">
                <i class="fas fa-angle-right"></i>
                <span class="nav-link-text">Quản lý đơn hàng</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="lien_he.php">
                <i class="fas fa-angle-right"></i>
                <span class="nav-link-text">Quản lý phản hồi</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" href="chi_nhanh.php">
                <i class="fas fa-angle-right"></i>
                <span class="nav-link-text">Quản lý chi nhánh</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="nhan_vien.php">
                <i class="fas fa-angle-right"></i>
                <span class="nav-link-text">Quản lý nhân viên</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="tuyen_dung.php">
                <i class="fas fa-angle-right"></i>
                <span class="nav-link-text">Quản lý tuyển dụng</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    <nav class="navbar navbar-top navbar-expand navbar-dark bg-primary border-bottom">
      <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <!-- Search form -->
          <form class="navbar-search navbar-search-light form-inline mr-sm-3" id="navbar-search-main">
            <div class="form-group mb-0">
              <div class="input-group input-group-alternative input-group-merge">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fas fa-search"></i></span>
                </div>
               <input class="form-control" placeholder="Nội dung tìm kiếm" type="text" name="search">
              </div>
            </div>
            <button type="button" class="close" data-action="search-close" data-target="#navbar-search-main" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </form>
          <!-- Navbar links -->
          <ul class="navbar-nav align-items-center  ml-md-auto ">
            <li class="nav-item d-xl-none">
              <!-- Sidenav toggler -->
              <div class="pr-3 sidenav-toggler sidenav-toggler-dark" data-action="sidenav-pin" data-target="#sidenav-main">
                <div class="sidenav-toggler-inner">
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                </div>
              </div>
            </li>
            <li class="nav-item d-sm-none">
              <a class="nav-link" href="#" data-action="search-show" data-target="#navbar-search-main">
                <i class="ni ni-zoom-split-in"></i>
              </a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="ni ni-bell-55"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-xl  dropdown-menu-right  py-0 overflow-hidden">
                <!-- Dropdown header -->
                <div class="px-3 py-3">
                  <h6 class="text-sm text-muted m-0">Bạn có <strong class="text-primary">0</strong> thông báo.</h6>
                </div>
            
                <a href="#!" class="dropdown-item text-center text-primary font-weight-bold py-3">Xem tất cả</a>
              </div>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="ni ni-ungroup"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-lg dropdown-menu-dark bg-default  dropdown-menu-right ">
                <div class="row shortcuts px-4">
                  <a href="#!" class="col-4 shortcut-item">
                    <span class="shortcut-media avatar rounded-circle bg-gradient-red">
                      <i class="ni ni-calendar-grid-58"></i>
                    </span>
                    <small>Calendar</small>
                  </a>
                  <a href="#!" class="col-4 shortcut-item">
                    <span class="shortcut-media avatar rounded-circle bg-gradient-orange">
                      <i class="ni ni-email-83"></i>
                    </span>
                    <small>Email</small>
                  </a>
                  <a href="#!" class="col-4 shortcut-item">
                    <span class="shortcut-media avatar rounded-circle bg-gradient-info">
                      <i class="ni ni-credit-card"></i>
                    </span>
                    <small>Payments</small>
                  </a>
                  <a href="#!" class="col-4 shortcut-item">
                    <span class="shortcut-media avatar rounded-circle bg-gradient-green">
                      <i class="ni ni-books"></i>
                    </span>
                    <small>Reports</small>
                  </a>
                  <a href="#!" class="col-4 shortcut-item">
                    <span class="shortcut-media avatar rounded-circle bg-gradient-purple">
                      <i class="ni ni-pin-3"></i>
                    </span>
                    <small>Maps</small>
                  </a>
                  <a href="#!" class="col-4 shortcut-item">
                    <span class="shortcut-media avatar rounded-circle bg-gradient-yellow">
                      <i class="ni ni-basket"></i>
                    </span>
                    <small>Shop</small>
                  </a>
                </div>
              </div>
            </li>
          </ul>
          <?php
              include('../../config.php');
              $sql = "
              SELECT ten_nguoi_dung,anh_minh_hoa
              from tbl_nguoi_dung where email='".$_SESSION['email']."' ";
              $quan_tri = mysqli_query($ket_noi, $sql);

              $row = mysqli_fetch_array($quan_tri);
           ;?>
          <ul class="navbar-nav align-items-center  ml-auto ml-md-0 ">
            <li class="nav-item dropdown">
              <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <div class="media align-items-center">
                  <span class="avatar avatar-sm rounded-circle">
                    <img alt="" src="<?php echo $row['anh_minh_hoa'] ? '../assets/img/theme/'.$row['anh_minh_hoa'] : 'images/abca.png' ;?>">
                  </span>
                  <div class="media-body  ml-2  d-none d-lg-block">
                    <span class="mb-0 text-sm  font-weight-bold"><?php echo $row["ten_nguoi_dung"];?></span>
                  </div>
                </div>
              </a>
              <div class="dropdown-menu  dropdown-menu-right ">
                <div class="dropdown-header noti-title">
                  <h6 class="text-overflow m-0">Welcome!</h6>
                </div>
                <div class="dropdown-divider"></div>
                <a href="dang_xuat.php" class="dropdown-item">
                  <i class="ni ni-user-run"></i>
                  <span>Đăng xuất</span>
                </a>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Quản lý hệ thống</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="../index.php"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="chi_nhanh.php">Quản lý chi nhánh</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Hệ thống cửa hàng </li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <a href="them_chi_nhanh.php" class="btn btn-sm btn-neutral">Thêm mới</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col">

          <div class="card">
            <!-- Card header -->
            <div class="card-header border-0">
              <h3 class="mb-0">Hệ thống cửa hàng</h3>
            </div>
            <!-- Light table -->
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">STT</th>
                    <th scope="col" class="sort" data-sort="name">Tên cửa hàng</th>
                    <th scope="col" class="sort" data-sort="name">Địa chỉ</th>
                    <th scope="col" class="sort" data-sort="name">Số điện thoại</th>
                    <th scope="col" class="sort" data-sort="status">Ảnh</th>
                    
                    <th scope="col">Sửa</th>
                    <th scope="col" class="sort" data-sort="completion">Xóa</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody class="list">
                    <?php
                    include("../../config.php");
                    if (isset($_GET["search"]) && $_GET["search"] != '') 
                    {
                        $search = $_GET["search"];
                        $sql="SELECT * FROM tbl_chi_nhanh 
                        WHERE dia_chi like '%$search%'
                        ORDER BY id_chi_nhanh DESC";
                    }
                    else
                    {
                      $sql="SELECT * FROM tbl_chi_nhanh ORDER BY id_chi_nhanh DESC";
                    }
                        $chi_nhanh=mysqli_query($ket_noi,$sql);
                        $i=0;
                        while ($row=mysqli_fetch_array($chi_nhanh))
                        {;?>
                        <tr>
                        <td><?php echo $i=$i+1;?></td>
                        <td><?php echo $row["ten_cua_hang"];?></td>
                        <td><?php echo $row["dia_chi"];?></td>
                        <td><?php echo $row["so_dien_thoai"];?></td>
                        <td><img src="<?php echo $row['anh'] ? "../assets/img/theme/".$row['anh'] : 'images/ĐTP.jpg' ;?>"< class="img-fluid" alt="Image" height='50.px'  width='100.px' ></td>
                        
                        <td><a href="chi_nhanh_sua.php?id=<?php echo $row["id_chi_nhanh"];?>">Sửa</a></td>
                        <td><a href="chi_nhanh_xoa.php?id=<?php echo $row["id_chi_nhanh"];?>">Xóa</a></td>
                        </tr>
                    <?php } ?>       
                </tbody>
              </table>
            </div>
            <!-- Card footer -->

          </div>
        </div>
      </div>
      <!-- Footer -->
      <footer class="footer pt-0">
        <div class="row align-items-center justify-content-lg-between">
          <div class="col-lg-6">
            <div class="copyright text-center  text-lg-left  text-muted">
              &copy; 2020 <a  class="font-weight-bold ml-1" target="_blank">Laika Cafe</a>
            </div>
          </div>
         
        </div>
      </footer>
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <!-- Argon JS -->
  <script src="../assets/js/argon.js?v=1.2.0"></script>
</body>

</html>