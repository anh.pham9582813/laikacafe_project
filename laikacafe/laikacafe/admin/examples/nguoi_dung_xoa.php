<?php
    session_start();
    if (!isset($_SESSION['email']))
    {
        echo "
                <script type='text/javascript'>
                    window.alert('Bạn không được phép truy cập');
                    window.location.href='dang_nhap.php';
                </script>
             ";
    }
;?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>Xóa người dùng</title>
  <!-- Favicon -->
  <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">

</head>

<body class="bg-default">
    <?php 
            include("../../config.php");

            $id_nguoi_dung = $_GET["id"];

            $sql = "
                DELETE 
                FROM `tbl_nguoi_dung` 
                WHERE `tbl_nguoi_dung`.`id_nguoi_dung` = '".$id_nguoi_dung."'
            ";

            $quan_tri = mysqli_query($ket_noi, $sql);

            echo "
                <script type='text/javascript'>
                    window.alert('Bạn đã xóa thành công');
                    window.location.href='nguoi_dung.php';
                </script>
            ";
        ;?>
</body>

</html>