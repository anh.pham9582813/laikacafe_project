<?php
    session_start();
    if (!isset($_SESSION['email']))
    {
        echo "
                <script type='text/javascript'>
                    window.alert('Bạn không được phép truy cập');
                    window.location.href='dang_nhap.php';
                </script>
             ";
    }
;?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>Cập nhật nhân viên</title>
  <!-- Favicon -->
  <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">
</head>

<body class="bg-default">
  <!-- Navbar -->
  <!-- Main content -->
  <div class="main-content">
    <!-- Header -->
    <div class="header bg-gradient-primary py-7 py-lg-8 pt-lg-9">
      <div class="container">
        <div class="header-body text-center mb-7">
          <div class="row justify-content-center">
            <div class="col-xl-5 col-lg-6 col-md-8 px-5">
              <h1 class="text-white">CẬP NHẬT NHÂN VIÊN</h1>
            </div>
          </div>
        </div>
      </div>
      <div class="separator separator-bottom separator-skew zindex-100">
        <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
          <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
        </svg>
      </div>
    </div>

    <?php
        include("../../config.php");
        $id_nhan_vien=$_GET["id"];

        $sql="SELECT * FROM tbl_nhan_vien WHERE id_nhan_vien = '".$id_nhan_vien."';";
        $nhan_vien=mysqli_query($ket_noi,$sql);
        $row=mysqli_fetch_array($nhan_vien);
    ;?>
    <!-- Page content -->
    <div class="container mt--8 pb-5">
      <!-- Table -->
      <div class="row justify-content-center">
        <div class="col-lg-6 col-md-8">
          <div class="card bg-secondary border-0">
            <div class="card-header bg-transparent pb-5">
            <div class="card-body px-lg-5 py-lg-5">
              <form role="form" method="Post" action="nhan_vien_sua_thuc_hien.php" enctype="multipart/form-data">
                <div class="form-group">
                  <label class="form-control-label" for="input-username">Tên nhân viên</label>
                  <input type="text" id="inputEmail" class="form-control" placeholder="Tên nhân viên" name="txtten" value="<?php echo $row["ten_nhan_vien"];?>">
                </div>
               
                <div class="form-group">
                  <label class="form-control-label" for="txtanh">Ảnh </label>
                  <input class="form-control" id="txtanh" type="file" placeholder="Ảnh " name="txtanh" value="<?php echo $row["anh"];?>">
                </div>

                 <div class="form-group">
                  <select name="txtbophan" id="" >
                      <option disabled selected> Bộ phận</option>
                       <option value="Thu Ngân">Thu Ngân</option>
                       <option value="Pha Chế">Pha Chế</option>
                       <option value="Phục vụ">Phục vụ</option>
                  </select>
                </div> 
                
                <div class="form-group">
                  <label class="form-control-label" for="input-username">Email</label>
                  <input type="text" id="inputEmail" class="form-control" placeholder="Email" name="txtemail" value="<?php echo $row["email"];?>">
                </div>

                <div class="form-group">
                  <label class="form-control-label" for="input-username">Mật khẩu</label>
                  <input type="text" id="inputEmail" class="form-control" placeholder="Mật khẩu" name="txtmatkhau" value="<?php echo $row["mat_khau"];?>">
                </div>

                <div class="text-center">
                    <input type="hidden" name="txtid" value="<?php echo $row["id_nhan_vien"];?>">
                    <input type="submit" class="btn btn-primary mt-4" name="btnSubmit" value="Cập nhập" />
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  <footer class="py-5" id="footer-main">
    <div class="container">
      <div class="row align-items-center justify-content-xl-between">
        <div class="col-xl-6">
          <div class="copyright text-center text-xl-left text-muted">
            &copy; 2020 <a href="../index.php" class="font-weight-bold ml-1" target="_blank">Laika Cafe</a>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <!-- Argon JS -->
  <script src="../assets/js/argon.js?v=1.2.0"></script>
</body>

</html>