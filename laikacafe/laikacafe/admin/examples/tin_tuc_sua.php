<?php
    session_start();
    if (!isset($_SESSION['email']))
    {
        echo "
                <script type='text/javascript'>
                    window.alert('Bạn không được phép truy cập');
                    window.location.href='dang_nhap.php';
                </script>
             ";
    }
;?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>Cập nhật tin tức</title>
  <!-- Favicon -->
  <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">

  <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
        <script>
          tinymce.init({
            selector: '#txtmota'
          });

          tinymce.init({
            selector: '#txtnoidung'
          });
        </script>
</head>

<body class="bg-default">
  <!-- Navbar -->
  <nav id="navbar-main" class="navbar navbar-horizontal navbar-transparent navbar-main navbar-expand-lg navbar-light">
    <div class="container">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="navbar-collapse navbar-custom-collapse collapse" id="navbar-collapse">
        <div class="navbar-collapse-header">
          <div class="row">
            <div class="col-6 collapse-close">
              <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
                <span></span>
                <span></span>
              </button>
            </div>
          </div>
        </div>
        <hr class="d-lg-none" />
      </div>
    </div>
  </nav>
  <!-- Main content -->
  <div class="main-content">
    <!-- Header -->
    <div class="header bg-gradient-primary py-7 py-lg-8 pt-lg-9">
      <div class="container">
        <div class="header-body text-center mb-7">
          <div class="row justify-content-center">
            <div class="col-xl-5 col-lg-6 col-md-8 px-5">
              <h1 class="text-white">CẬP NHẬT TIN TỨC</h1>
            </div>
          </div>
        </div>
      </div>
      <div class="separator separator-bottom separator-skew zindex-100">
        <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
          <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
        </svg>
      </div>
    </div>
    <!-- Page content -->
    <div class="container mt--8 pb-5">
      <!-- Table -->
      <div class="row justify-content-center">
        <div class="col-lg-6 col-md-8">
          <div class="card bg-secondary border-0">


            <?php 
                include("../../config.php");

                $id_tin_tuc = $_GET["id"];

                $sql = "
                          SELECT * 
                          FROM tbl_tin_tuc
                          WHERE id_tin_tuc = '".$id_tin_tuc."'
                ";

                $tin_tuc = mysqli_query($ket_noi, $sql);

                $row = mysqli_fetch_array($tin_tuc);
            ;?>


            <div class="card-body px-lg-5 py-lg-5">
            
                <form role="form" method="POST" action="tin_tuc_sua_thuc_hien.php" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="form-control-label" for="input-username">Tiêu đề</label>
                        <input type="text" id="inputEmail" class="form-control" placeholder="Tiêu đề" name="txttieude" value="<?php echo $row["tieu_de"];?>">
                    </div>
                    <div class="form-group">
                      <label class="form-control-label" for="txtanh">Ảnh minh họa</label>
                      <input class="form-control" id="txtanh" type="file" placeholder="Ảnh minh họa" name="txtanh" value="<?php echo $row["anh_minh_hoa"];?>">
                    </div>
                    <div class="form-group">
                      <label class="form-control-label" for="input-first-name">Mô tả</label>
                      <textarea type="text" class="form-control" placeholder="Mô tả" name="txtmota" id="txtmota"><?php echo $row["mo_ta"];?></textarea>
                    </div>
                    <div class="form-group">
                      <label class="form-control-label" for="input-first-name">Nội dung</label>
                      <textarea type="text" class="form-control" placeholder="Nội dung" name="txtnoidung" id="txtnoidung"><?php echo $row["noi_dung"];?></textarea>
                    </div>
                    <div class="text-center">
                      <input type="hidden" name="txtid" value="<?php echo $row["id_tin_tuc"];?>">
                      <input type="submit" class="btn btn-primary my-4" value="Cập nhật">
                    </div>
                </form>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  <footer class="py-5" id="footer-main">
    <div class="container">
      <div class="row align-items-center justify-content-xl-between">
        <div class="col-xl-6">
          <div class="copyright text-center text-xl-left text-muted">
            &copy; 2020 <a>Laika Cafe</a>
          </div>
        </div>
        <div class="col-xl-6">
        </div>
      </div>
    </div>
  </footer>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <!-- Argon JS -->
  <script src="../assets/js/argon.js?v=1.2.0"></script>
</body>

</html>