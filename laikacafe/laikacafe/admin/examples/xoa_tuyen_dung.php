<?php
    session_start();
    if (!isset($_SESSION['email']))
    {
        echo "
                <script type='text/javascript'>
                    window.alert('Bạn không được phép truy cập');
                    window.location.href='dang_nhap.php';
                </script>
             ";
    }
;?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Xóa tuyển dụng</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link href="css/styles.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="sb-nav-fixed">
        <?php
            
            include("../../config.php");
             $id_tuyen_dung=$_GET["id"];

             $sql=" DELETE FROM `tbl_tuyen_dung` WHERE `tbl_tuyen_dung`.`id_tuyen_dung`='".$id_tuyen_dung."'";
             $tuyen_dung=mysqli_query($ket_noi,$sql);

             echo "
                <script type='text/javascript'>
                    window.alert('Bạn đã xóa thành công');
                    window.location.href='tuyen_dung.php';
                </script>
             "
        ;?>                 
    </body>
</html>
