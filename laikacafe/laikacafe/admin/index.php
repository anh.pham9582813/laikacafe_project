<?php
    session_start();
    if (!isset($_SESSION['email']))
    {
        echo "
                <script type='text/javascript'>
                    window.alert('Bạn không được phép truy cập');
                    window.location.href='examples/dang_nhap.php';
                </script>
             ";
    }
;?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>Quản lý hệ thống</title>
  <!-- Favicon -->
  <link rel="icon" href="assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <!-- Page plugins -->
  <!-- Argon CSS -->
  <link rel="stylesheet" href="assets/css/argon.css?v=1.2.0" type="text/css">
</head>

<body>
<?php  
                   // 1. Load file cấu hình để kết nối đến máy chủ csdl và csdl
                    include("../config.php");
                   
                   // 2. Lấy ra dữ liệu mong muốn (tin tức đã lưu trong csdl)
                    $sql1 =  "select *from tbl_nguoi_dung"; 
                    $sql2 =  "select *from tbl_tin_tuc"; 
                    $sql3 =  "select * from tbl_san_pham";
                    $sql4 =  "select * from tbl_dat_hang";
                    $sql5 = "select * from tbl_phan_hoi";
                    $sql6 = "select * from tbl_nhan_vien";
                    $sql7 = "select * from tbl_chi_nhanh";
                    $sql8 = "select * from tbl_tuyen_dung";


                    // 3. Thực thi câu lệnh lấy dữ liệu mong muốn
                    $noi_dung_tin_tuc = mysqli_query($ket_noi, $sql2);
                    $noi_dung_nguoi_dung = mysqli_query($ket_noi, $sql1);
                    $noi_dung_san_pham = mysqli_query($ket_noi, $sql3);
                    $noi_dung_dat_hang = mysqli_query($ket_noi, $sql4);
                    $noi_dung_phan_hoi = mysqli_query($ket_noi, $sql5);
                    $noi_dung_nhan_vien = mysqli_query($ket_noi, $sql6);
                    $noi_dung_chi_nhanh = mysqli_query($ket_noi, $sql7);
                    $noi_dung_tuyen_dung = mysqli_query($ket_noi, $sql8);

                    // 4. Hiển thị ra dữ liệu mà các bạn vừa lấy
                    $so_luong_tin_tuc = mysqli_num_rows($noi_dung_tin_tuc);
                    $so_luong_nguoi_dung = mysqli_num_rows($noi_dung_nguoi_dung);
                    $so_luong_san_pham = mysqli_num_rows($noi_dung_san_pham);
                    $so_luong_don = mysqli_num_rows($noi_dung_dat_hang);
                    $so_luong_phan_hoi = mysqli_num_rows($noi_dung_phan_hoi);
                    $so_luong_nhan_vien = mysqli_num_rows($noi_dung_nhan_vien);
                    $so_luong_chi_nhanh = mysqli_num_rows($noi_dung_chi_nhanh);
                    $so_luong_tuyen_dung = mysqli_num_rows($noi_dung_tuyen_dung);
           
        ;?>
  <!-- Sidenav -->
  <nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <div class="sidenav-header  align-items-center">
        <a class="navbar-brand" href="index.php">
          <img src="assets/img/brand/abca.png" class="navbar-brand-img" alt="...">
        </a>
      </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link active" href="index.php">
                <i class="ni ni-tv-2 text-primary"></i>
                <span class="nav-link-text">Quản lý hệ thống</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="examples/nguoi_dung.php">
                <i class="fas fa-angle-right"></i>
                <span class="nav-link-text">Quản lý người dùng</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="examples/tin_tuc.php">
                <i class="fas fa-angle-right"></i>
                <span class="nav-link-text">Quản lý tin tức</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="examples/san_pham.php">
                <i class="fas fa-angle-right"></i>
                <span class="nav-link-text">Quản lý sản phẩm</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="examples/dat_hang_ex.php">
                <i class="fas fa-angle-right"></i>
                <span class="nav-link-text">Quản lý đơn hàng</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="examples/lien_he.php">
                <i class="fas fa-angle-right"></i>
                <span class="nav-link-text">Quản lý phản hồi</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="examples/chi_nhanh.php">
                <i class="fas fa-angle-right"></i>
                <span class="nav-link-text">Quản lý chi nhánh</span>
              </a>
            </li>  
            <li class="nav-item">
              <a class="nav-link" href="examples/nhan_vien.php">
                <i class="fas fa-angle-right"></i>
                <span class="nav-link-text">Quản lý nhân viên</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="examples/tuyen_dung.php">
                <i class="fas fa-angle-right"></i>
                <span class="nav-link-text">Quản lý tuyển dụng</span>
              </a>
            </li>    
          </ul>
          <!-- Divider -->
          <hr class="my-3">
   
          
        </div>
      </div>
    </div>
  </nav>
  <!-- Main content -->
  <div class="main-content" style="background: linear-gradient(87deg, #ff9c00 0, #ffb800 100%) !important ; height: 100vh" id="panel">
    <!-- Topnav -->
    <nav class="navbar navbar-top navbar-expand navbar-dark bg-primary border-bottom">
      <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <!-- Search form -->
          <form class="navbar-search navbar-search-light form-inline mr-sm-3" id="navbar-search-main">
            <div class="form-group mb-0">
              <div class="input-group input-group-alternative input-group-merge">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fas fa-search"></i></span>
                </div>
                <input class="form-control" placeholder="Nội dung tìm kiếm" type="text">
              </div>
            </div>
            <button type="button" class="close" data-action="search-close" data-target="#navbar-search-main" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </form>
          <!-- Navbar links -->
          <ul class="navbar-nav align-items-center  ml-md-auto ">
            <li class="nav-item d-xl-none">
              <!-- Sidenav toggler -->
              <div class="pr-3 sidenav-toggler sidenav-toggler-dark" data-action="sidenav-pin" data-target="#sidenav-main">
                <div class="sidenav-toggler-inner">
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                </div>
              </div>
            </li>
            <li class="nav-item d-sm-none">
              <a class="nav-link" href="#" data-action="search-show" data-target="#navbar-search-main">
                <i class="ni ni-zoom-split-in"></i>
              </a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="ni ni-bell-55"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-xl  dropdown-menu-right  py-0 overflow-hidden">
                <!-- Dropdown header -->
                <div class="px-3 py-3">
                  <h6 class="text-sm text-muted m-0">Bạn có <strong class="text-primary">0</strong> thông báo.</h6>
                </div>
                <!-- List group -->
                <div class="list-group list-group-flush">
                                </div>
                <!-- View all -->
                <a href="#!" class="dropdown-item text-center text-primary font-weight-bold py-3">Xem tất cả</a>
              </div>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="ni ni-ungroup"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-lg dropdown-menu-dark bg-default  dropdown-menu-right ">
                <div class="row shortcuts px-4">
                  <a href="#!" class="col-4 shortcut-item">
                    <span class="shortcut-media avatar rounded-circle bg-gradient-red">
                      <i class="ni ni-calendar-grid-58"></i>
                    </span>
                    <small>Calendar</small>
                  </a>
                  <a href="#!" class="col-4 shortcut-item">
                    <span class="shortcut-media avatar rounded-circle bg-gradient-orange">
                      <i class="ni ni-email-83"></i>
                    </span>
                    <small>Email</small>
                  </a>
                  <a href="#!" class="col-4 shortcut-item">
                    <span class="shortcut-media avatar rounded-circle bg-gradient-info">
                      <i class="ni ni-credit-card"></i>
                    </span>
                    <small>Payments</small>
                  </a>
                  <a href="#!" class="col-4 shortcut-item">
                    <span class="shortcut-media avatar rounded-circle bg-gradient-green">
                      <i class="ni ni-books"></i>
                    </span>
                    <small>Reports</small>
                  </a>
                  <a href="#!" class="col-4 shortcut-item">
                    <span class="shortcut-media avatar rounded-circle bg-gradient-purple">
                      <i class="ni ni-pin-3"></i>
                    </span>
                    <small>Maps</small>
                  </a>
                  <a href="#!" class="col-4 shortcut-item">
                    <span class="shortcut-media avatar rounded-circle bg-gradient-yellow">
                      <i class="ni ni-basket"></i>
                    </span>
                    <small>Shop</small>
                  </a>
                </div>
              </div>
            </li>
          </ul>
          <?php
             $sql = " SELECT ten_nguoi_dung,anh_minh_hoa from tbl_nguoi_dung where email='".$_SESSION['email']."'";
                    $quan_tri = mysqli_query($ket_noi, $sql);

                    $row = mysqli_fetch_array($quan_tri);
          ;?>
          <ul class="navbar-nav align-items-center  ml-auto ml-md-0 ">
            <li class="nav-item dropdown">
              <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <div class="media align-items-center">
                  <span class="avatar avatar-sm rounded-circle">
                    <img alt="Image placeholder" src="<?php echo $row['anh_minh_hoa'] ? 'assets/img/theme/'.$row['anh_minh_hoa'] : 'images/abca.png' ;?>">
                  </span>
                  <div class="media-body  ml-2  d-none d-lg-block">
                    <span class="mb-0 text-sm  font-weight-bold"><?php echo $row["ten_nguoi_dung"];?></span>
                  </div>
                </div>
              </a>
              <div class="dropdown-menu  dropdown-menu-right ">
                <div class="dropdown-header noti-title">
                  <h6 class="text-overflow m-0">Welcome!</h6>
                </div>
                
                <div class="dropdown-divider"></div>
                <a href="examples/dang_xuat.php" class="dropdown-item">
                  <i class="ni ni-user-run"></i>
                  <span>Đăng xuất</span>
                </a>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Quản lý hệ thống</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="index.php"><i class="fas fa-home"></i></a></li>                 
                </ol>
              </nav>
            </div>
            
          </div>
          <!-- Card stats -->
          
            
          <div class="row">
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                   <div class="col">
                          <h5 class="card-title text-uppercase text-muted mb-0">Quản lý người dùng</h5>
                          <span class="h2 font-weight-bold mb-0"><?php echo $so_luong_nguoi_dung; ?> người</span>
                          <div class="card-footer d-flex align-items-center justify-content-between">
                          <a class="small text-black stretched-link" href="examples/nguoi_dung.php">Chi tiết</a>
                          <div class="small text-black"><i class="fas fa-angle-right"></i></div>
                          </div>
                      </div>
                      <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                        <i class="ni ni-active-40"></i>
                      </div>
                    </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                          <h5 class="card-title text-uppercase text-muted mb-0">Quản lý chi nhánh</h5>
                          <span class="h2 font-weight-bold mb-0"><?php echo $so_luong_chi_nhanh; ?> chi nhánh</span>
                          <div class="card-footer d-flex align-items-center justify-content-between">
                          <a class="small text-black stretched-link" href="examples/nhan_vien.php">Chi tiết</a>
                          <div class="small text-black"><i class="fas fa-angle-right"></i></div>
                          </div>
                    </div>
                      <div class="col-auto">
                        <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                          <i class="ni ni-chart-bar-32"></i>
                        </div>
                    </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                          <h5 class="card-title text-uppercase text-muted mb-0">Quản lý sản phẩm</h5>
                          <span class="h2 font-weight-bold mb-0"><?php echo $so_luong_san_pham; ?> sản phẩm</span>
                          <div class="card-footer d-flex align-items-center justify-content-between">
                          <a class="small text-black stretched-link" href="examples/san_pham.php">Chi tiết</a>
                          <div class="small text-black"><i class="fas fa-angle-right"></i></div>
                          </div>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                        <i class="ni ni-money-coins"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                          <h5 class="card-title text-uppercase text-muted mb-0">Quản lý phản hồi</h5>
                          <span class="h2 font-weight-bold mb-0"><?php echo $so_luong_phan_hoi; ?> phản hồi</span>
                          <div class="card-footer d-flex align-items-center justify-content-between">
                          <a class="small text-black stretched-link" href="examples/lien_he.php">Chi tiết</a>
                          <div class="small text-black"><i class="fas fa-angle-right"></i></div>
                          </div>
                    </div>
                      <div class="col-auto">
                        <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                          <i class="ni ni-chart-bar-32"></i>
                        </div>
                    </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                          <h5 class="card-title text-uppercase text-muted mb-0">Quản lý đơn hàng</h5>
                          <span class="h2 font-weight-bold mb-0"><?php echo $so_luong_don; ?> đơn</span>
                          <div class="card-footer d-flex align-items-center justify-content-between">
                          <a class="small text-black stretched-link" href="examples/dat_hang_ex.php">Chi tiết</a>
                          <div class="small text-black"><i class="fas fa-angle-right"></i></div>
                          </div>
                      </div>
                      <div class="col-auto">
                        <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                          <i class="ni ni-chart-pie-35"></i>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                          <h5 class="card-title text-uppercase text-muted mb-0">Quản lý nhân viên</h5>
                          <span class="h2 font-weight-bold mb-0"><?php echo $so_luong_nhan_vien; ?> nhân viên</span>
                          <div class="card-footer d-flex align-items-center justify-content-between">
                          <a class="small text-black stretched-link" href="examples/nhan_vien.php">Chi tiết</a>
                          <div class="small text-black"><i class="fas fa-angle-right"></i></div>
                          </div>
                    </div>
                      <div class="col-auto">
                        <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                          <i class="ni ni-chart-bar-32"></i>
                        </div>
                    </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                          <h5 class="card-title text-uppercase text-muted mb-0">Quản lý tin tức</h5>
                          <span class="h2 font-weight-bold mb-0"><?php echo $so_luong_tin_tuc; ?> tin tức</span>
                          <div class="card-footer d-flex align-items-center justify-content-between">
                          <a class="small text-black stretched-link" href="examples/tin_tuc.php">Chi tiết</a>
                          <div class="small text-black"><i class="fas fa-angle-right"></i></div>
                          </div>
                      </div>
                      <div class="col-auto">
                        <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                          <i class="ni ni-chart-pie-35"></i>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                   <div class="col">
                          <h5 class="card-title text-uppercase text-muted mb-0">Quản lý tuyển dụng</h5>
                          <span class="h2 font-weight-bold mb-0"><?php echo $so_luong_tuyen_dung; ?> đơn</span>
                          <div class="card-footer d-flex align-items-center justify-content-between">
                          <a class="small text-black stretched-link" href="examples/tuyen_dung.php">Chi tiết</a>
                          <div class="small text-black"><i class="fas fa-angle-right"></i></div>
                          </div>
                      </div>
                      <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                        <i class="ni ni-active-40"></i>
                      </div>
                    </div>
                    </div>
                </div>
              </div>
            </div>
            
                <!-- Card body -->

                
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->

  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <!-- Optional JS -->
  <script src="assets/vendor/chart.js/dist/Chart.min.js"></script>
  <script src="assets/vendor/chart.js/dist/Chart.extension.js"></script>
  <!-- Argon JS -->
  <script src="assets/js/argon.js?v=1.2.0"></script>
</body>

</html>
