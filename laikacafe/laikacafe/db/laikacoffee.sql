-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 01, 2023 at 05:31 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.3.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laikacoffee`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_chi_nhanh`
--

CREATE TABLE `tbl_chi_nhanh` (
  `id_chi_nhanh` int(11) NOT NULL,
  `ten_cua_hang` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `dia_chi` text COLLATE utf8_unicode_ci NOT NULL,
  `so_dien_thoai` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `anh` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_chi_nhanh`
--

INSERT INTO `tbl_chi_nhanh` (`id_chi_nhanh`, `ten_cua_hang`, `dia_chi`, `so_dien_thoai`, `anh`) VALUES
(1, 'LAIKA Cafe', '65 Đường Láng-Đống Đa-Hà Nội', 'SĐT: +84912345677', 'LK1.jpg'),
(2, 'LAIKA Cafe', '52 Lĩnh Nam-Hoàng Mai-Hà Nội', 'SĐT: +84912342213', 'LK2.jpg'),
(4, 'LAIKA Cafe', '411-Hạ Long-Quảng Ninh', 'SĐT: +84912345612', 'LK4.jpg'),
(5, 'LAIKA Cafe', '112 Lê Hồng Phong-TP Vinh-Nghệ An', 'SĐT: +84912345615', 'LK9.jpg'),
(6, 'LAIKA Cafe', '102 Quang Trung-Tuyên Quang', 'SĐT: +84912345677', 'LK6.jpg'),
(13, 'LAIKA Cafe', '229 Phố Huế-Hai Bà Trưng-Hà Nội', 'SĐT: +84912345624', 'LK10.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dat_hang`
--

CREATE TABLE `tbl_dat_hang` (
  `id_dat_hang` int(11) NOT NULL,
  `id_chi_nhanh` int(11) NOT NULL,
  `ngay_dat` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `ten_khach_hang` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `so_dien_thoai` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `dia_chi` text COLLATE utf8_unicode_ci NOT NULL,
  `id_san_pham` int(11) NOT NULL,
  `so_luong` int(5) NOT NULL DEFAULT 0,
  `size` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ghi_chu` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_dat_hang`
--

INSERT INTO `tbl_dat_hang` (`id_dat_hang`, `id_chi_nhanh`, `ngay_dat`, `ten_khach_hang`, `so_dien_thoai`, `dia_chi`, `id_san_pham`, `so_luong`, `size`, `email`, `ghi_chu`) VALUES
(1, 1, '2023-03-29 08:14:29', 'Nguyễn Thị Hằng', '0123456789', '23 Chùa Bộc, Trung Liệt, Đống Đa, Hà Nội', 1, 1, 'L', 'hang12.@gmail.com', NULL),
(2, 0, '2021-10-26 17:51:22', 'Mai Hương', '0123456887', '33 Chùa Bộc, Trung Liệt, Đống Đa, Hà Nội', 2, 1, 'L', 'mh12@gmail.com', NULL),
(3, 0, '2021-10-26 17:38:19', 'Phạm Thùy Linh', '0123456798', '56 Chùa Bộc, Trung Liệt, Đống Đa, Hà Nội', 3, 1, 'L', 'ptl@gmail.com', NULL),
(4, 0, '2021-10-26 18:33:37', 'Ngọc Anh', '0123456778', '12 Chùa Bộc, Trung Liệt, Đống Đa, Hà Nội', 4, 1, 'L', 'ngocanh@gmail.com', NULL),
(5, 0, '2021-10-26 18:33:02', 'Hoài Trang', '0123455678', '11 Chùa Bộc, Trung Liệt, Đống Đa, Hà Nội', 3, 1, 'L', 'tht@gmail.com', NULL),
(6, 0, '2021-10-26 18:33:25', 'Ngọc Ánh', '0123456768', '55 Chùa Bộc, Trung Liệt, Đống Đa, Hà Nội', 2, 1, 'S', 'ptna@gmail.com', NULL),
(7, 0, '2021-10-26 18:30:31', 'Thanh Lan', '0125467896', '10 Chùa Bộc, Trung Liệt, Đống Đa, Hà Nội', 1, 1, 'S', 'thanhlan@gmail.com', NULL),
(8, 0, '2021-10-26 18:32:31', 'Thu Phương', '0123234564', '67 Chùa Bộc, Trung Liệt, Đống Đa, Hà Nội', 2, 1, 'S', 'dtp@gmail.com', NULL),
(9, 0, '2021-10-26 18:32:25', 'Mai Anh', '0123324357', '12 Chùa Bộc, Trung Liệt, Đống Đa, Hà Nội', 1, 1, 'S', 'pma@gmail.com', NULL),
(10, 0, '2021-10-26 18:32:14', 'Lan Anh', '0123456987', '66 Chùa Bộc, Trung Liệt, Đống Đa, Hà Nội', 2, 2, 'S', 'ntla@gmail.com', NULL),
(11, 0, '2021-10-26 18:52:19', 'Huyền Trang', '0123234567', ' Chùa Bộc, Trung Liệt, Đống Đa, Hà Nội', 1, 2, 'M', 'ttht@gmail.com', NULL),
(12, 0, '2021-10-30 03:17:15', 'Mai Linh', '0123243565', ' 99 Chùa Bộc, Trung Liệt, Đống Đa, Hà Nội', 4, 2, 'M', 'mailinh@gmail.com', ''),
(13, 0, '2021-11-01 04:02:24', 'THU PHƯƠNG', '0123456789', '31 TRƯỜNG CHINH, NAM ĐỊNH', 7, 2, 'S', 'DTP@GMAIL.COM', NULL),
(14, 0, '2021-11-01 04:14:31', 'Minh', '0292873586', '12 Mai Quốc Việt, Ba Đình', 7, 2, 'M', 'minhminh@gmail.com', NULL),
(15, 0, '2021-11-01 04:25:06', 'Hoàng Anh', '0123456789', '123 Trần Bình, Mai Dịch, Cầu Giấy, Hà Nội', 12, 1, 'S', 'hanh@gmail.com', NULL),
(16, 0, '2021-11-01 04:33:11', 'Hoàng Anh', '0123456789', '123 Trần Bình, Mai Dịch, Cầu Giấy, Hà Nội', 1, 1, 'S', 'hanh@gmail.com', NULL),
(17, 0, '2021-11-01 04:34:04', 'Hoàng Mai', '0321234546', '111 Trần Bình, Mai Dịch, Cầu Giấy, Hà Nội', 5, 1, 'M', 'hmai@gmail.com', NULL),
(18, 0, '2021-11-01 04:56:42', 'Thanh Huyền', '0932433312', '12 Chùa Bộc, Đống Đa, Hà Nội', 7, 1, 'M', 'thuyen@gmail.com', NULL),
(19, 0, '2021-11-02 09:42:50', 'thu ', '0123456789', '31 TRƯỜNG CHINH, NAM ĐỊNH', 13, 1, 'M', 'DTP@GMAIL.COM', NULL),
(20, 0, '2021-11-02 09:54:58', 'Thanh Thu', '0212345465', '123 Trần Bình, Mai Dịch, Cầu Giấy, Hà Nội', 6, 1, 'S', 'tt@gmail.com', NULL),
(21, 0, '2021-11-02 09:57:27', 'Mai An', '0943674523', '594 Láng, Đống Đa', 5, 1, 'S', 'Maian@gmail.com', NULL),
(23, 0, '2021-11-03 04:11:13', 'THU PHƯƠNG', '0987654321', '31 TRƯỜNG CHINH, thái bình', 18, 2, 'S', 'DTP@GMAIL.COM', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nguoi_dung`
--

CREATE TABLE `tbl_nguoi_dung` (
  `id_nguoi_dung` int(11) NOT NULL,
  `id_chi_nhanh` int(11) NOT NULL,
  `ten_nguoi_dung` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `so_dien_thoai` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `mat_khau` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `anh_minh_hoa` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ghi_chu` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_nguoi_dung`
--

INSERT INTO `tbl_nguoi_dung` (`id_nguoi_dung`, `id_chi_nhanh`, `ten_nguoi_dung`, `email`, `so_dien_thoai`, `mat_khau`, `anh_minh_hoa`, `ghi_chu`) VALUES
(1, 0, 'Hoài Trang', 'anhoaitran55@gmail.com', '0985239123', '123456', 'VFPH0297.jpg', NULL),
(2, 0, 'Nguyễn Thị Lan Anh', 'lananhnguyen@gmail.com', '0987486465', '000001', 'NTLA.jpg', NULL),
(4, 0, 'Phạm Mai Anh', 'pmanh@gmail.com', '0986792001', '654321', 'pma.jpg', NULL),
(5, 0, 'Trang Trịnh', 'ttht@gmail.com', '0993853687', '123456', 'TTHT.jpg', NULL),
(8, 0, 'Đinh Thu Phương', 'dtp1234@gmail.com', '0993853687', '111111', 'TP.jpg', NULL),
(12, 0, 'Nguyễn Hoàng Thu Trang', 'nhtt@gmail.com', '0978452156', '123456', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nhan_vien`
--

CREATE TABLE `tbl_nhan_vien` (
  `id_nhan_vien` int(11) NOT NULL,
  `id_chi_nhanh` int(11) NOT NULL,
  `ten_nhan_vien` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `anh` text COLLATE utf8_unicode_ci NOT NULL,
  `bo_phan` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `mat_khau` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_nhan_vien`
--

INSERT INTO `tbl_nhan_vien` (`id_nhan_vien`, `id_chi_nhanh`, `ten_nhan_vien`, `anh`, `bo_phan`, `email`, `mat_khau`) VALUES
(1, 0, 'Phan Thị Ngọc Ánh', 'PTNA1.jpg', 'Phục vụ', 'pa16911@gmail.com', '169011'),
(2, 0, 'Nguyễn Thị Dịu', 'NTD1.jpg', 'Pha Chế', 'diu4101@gmail.com', '4101'),
(3, 0, 'Phạm Thùy Linh', 'PTL.jpg', 'Thu Ngân', 'ptl1811@gmail.com', '1811');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_phan_hoi`
--

CREATE TABLE `tbl_phan_hoi` (
  `id_phan_hoi` int(11) NOT NULL,
  `id_nhan_vien` int(11) NOT NULL,
  `thoi_gian_gui` timestamp NOT NULL DEFAULT current_timestamp(),
  `ten_nguoi_gui` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `email_lien_he` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `noi_dung` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_phan_hoi`
--

INSERT INTO `tbl_phan_hoi` (`id_phan_hoi`, `id_nhan_vien`, `thoi_gian_gui`, `ten_nguoi_gui`, `email_lien_he`, `noi_dung`) VALUES
(5, 2, '2021-10-29 09:30:52', 'Phạm Mai Vàng', 'phamvangvang79@gmail.com', 'Đồ uống rất ngon, pha chế rất có tâm, mình cảm giác được sự chu đáo trong từng đồ của quán. Mình sẽ ủng hộ LAIKA dài dài'),
(9, 3, '2021-10-29 13:33:08', 'Nguyễn Nguyệt Anh', 'nganh456@gmail.com', 'Anh phục vụ đẹp trai quá, cười lên tỏa nắng :v '),
(10, 2, '2021-10-29 13:34:09', 'Trần Thị Dung', 'dungtran841@gmail.com', 'Capuchino hơi ngọt, bánh  rất ngon '),
(11, 3, '2021-10-29 13:44:30', 'Trịnh Huyền Trang', 'huyntragnam@gmail.com', 'ANh phục vụ đẹp trai quasaaa, xin inf ạ'),
(12, 2, '2021-10-30 13:46:30', 'Khánh Ngân', 'kngan2003@gmail.com', 'Phục vụ rất chu đáo, nhanh chóng <3'),
(14, 1, '2021-10-31 08:24:24', 'Hoài An', 'anthui123@gmail.com', 'A pha chế tốt bụng'),
(15, 1, '2021-11-01 04:12:35', 'Quốc Anh Welax', 'quocanh@gmail.com', 'Ngon'),
(16, 1, '2021-11-01 04:26:46', 'Hoàng Anh', 'hanh@gmail.com', 'Pha chế ngon'),
(17, 1, '2021-11-01 04:58:55', 'Hoàng anh', 'hanh@gmail.com', 'pha chế ngon'),
(18, 2, '2021-11-02 02:39:57', 'Nguyệt Anh', 'nguyetaanh272@gmail.com', 'phục vụ tốt, chuyên nghiệp'),
(19, 1, '2021-11-02 09:48:10', 'phương', 'dtp@gmail.com', 'pha chế rất ngon'),
(20, 1, '2021-11-02 12:51:56', 'Nguyễn Ngọc Ngạn ', 'ngocngan@gmail.com', 'đồ uống ngon'),
(21, 2, '2021-11-02 13:00:52', 'Mai An', 'maian@gmail.com', 'Phục vụ tốt'),
(22, 2, '2021-11-02 13:14:43', 'Bún', 'hihi123@gmail.com', 'phục vụ tốt'),
(23, 2, '2021-11-02 13:30:10', 'Đậu ', 'bundau@gmail.com', 'tốt nhanh'),
(24, 3, '2021-11-02 13:38:23', 'Mắm Tôm', 'bdmt@gmail.com', 'Bạn này xinh xắn');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_san_pham`
--

CREATE TABLE `tbl_san_pham` (
  `id_san_pham` int(11) NOT NULL,
  `ten_san_pham` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mo_ta` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `gia_tien` int(20) NOT NULL,
  `anh_minh_hoa` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_san_pham`
--

INSERT INTO `tbl_san_pham` (`id_san_pham`, `ten_san_pham`, `mo_ta`, `gia_tien`, `anh_minh_hoa`) VALUES
(1, 'Cà phê ủ lạnh - Dâu tây', 'Thơm vị dâu hoà cùng vị chua ngọt.', 59000, 'menu9.jpg'),
(2, 'Cà phê ủ lạnh - Cherry', 'Vị cherry ngọt ngào không thể chối từ.', 59000, 'menu5.jpg'),
(3, 'Bina Apple Green', 'Nước táo xanh thơm lừng thanh mát.', 49000, 'menu13.png'),
(4, 'Cà phê ủ lạnh - Cam vàng', 'Thơm vị cam tươi mát.', 59000, 'menu4.jpg'),
(5, 'Trà Cherry', 'Trà ngọt ngào như những nàng công chúa.', 45000, 'menu11.jpg'),
(6, 'Cà phê kem', 'Hương cà phê hoà cùng lớp kem đặc.', 45000, 'menu6.jpg'),
(7, 'Cà phê cốt dừa', ' Vị đậm cà phê với dừa', 45000, 'menu3.jpg'),
(8, 'Trà chanh', 'Vị thanh của trà cùng vị chua.', 30000, 'menu12.jpg'),
(9, 'Trà bưởi', 'Vị chua ngọt tự nhiên của bưởi', 40000, 'menu8.jpg'),
(10, 'Very berry - Kem sữa', 'Thơm ngọt.', 59000, 'menu14.jpg'),
(11, 'Cà phê matcha', 'Vị cà phê cùng vị matcha thơm lừng.', 52000, 'menu2.jpg'),
(12, 'Cà phê Đen', 'Đậm đà hương vị thuần túy', 40000, 'menu1.jpg'),
(13, 'Cà phê bạc sỉu', 'Hương cà phê béo thơm.', 40000, 'menu15.jpg'),
(14, 'Kem matcha', 'Thơm mát vị matcha', 45000, 'menu10.jpg'),
(18, 'Cherry Spring', 'Vị mới khá độc lạ.', 59000, 'menu16.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tin_tuc`
--

CREATE TABLE `tbl_tin_tuc` (
  `id_tin_tuc` int(11) NOT NULL,
  `id_chi_nhanh` int(11) NOT NULL,
  `tieu_de` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `mo_ta` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `noi_dung` text COLLATE utf8_unicode_ci NOT NULL,
  `anh_minh_hoa` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `so_lan_doc` int(11) NOT NULL DEFAULT 0,
  `ngay_dang` timestamp NOT NULL DEFAULT current_timestamp(),
  `ghi_chu` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_tin_tuc`
--

INSERT INTO `tbl_tin_tuc` (`id_tin_tuc`, `id_chi_nhanh`, `tieu_de`, `mo_ta`, `noi_dung`, `anh_minh_hoa`, `so_lan_doc`, `ngay_dang`, `ghi_chu`) VALUES
(1, 0, 'Laika Cafe Trung Hòa khiến bao người xuyến xao bởi không gian cực xinh xắn', '<p>Địa chỉ: 91 Trung H&ograve;a, Y&ecirc;n H&ograve;a, Cầu Giấy</p>\r\n<p>Laika Cafe cơ sở mới quả kh&ocirc;ng l&agrave;m ch&uacute;ng ta thất vọng bởi kh&ocirc;ng gian cực xinh, cho bạn chill hết nấc v&agrave; sống ảo thoải m&aacute;i. C&ugrave;ng t&igrave;m hiểu ngay với TravelMag.</p>', '<p style=\"box-sizing: border-box; text-rendering: optimizelegibility; font-size: 17px; line-height: 26px; color: #212529; text-align: justify; background-color: #ffffff; font-family: NotoSerif !important; margin: 0px 0px !important 8px 0px !important;\">Qu&aacute;n sử dụng tone m&agrave;u x&aacute;m gỗ để tạo n&ecirc;n sự h&agrave;i h&ograve;a trong nhiều phong c&aacute;ch v&agrave; kh&ocirc;ng gian. Hiện đại c&oacute;, nhẹ nh&agrave;ng c&oacute;, sang chảnh c&oacute;, kiểu teen teen theo xu thế cũng c&oacute;. Sống ảo ở đ&acirc;y th&igrave; đ&uacute;ng l&agrave; kh&ocirc;ng thiếu một concept n&agrave;o cả.</p>\r\n<p style=\"box-sizing: border-box; text-rendering: optimizelegibility; font-size: 17px; line-height: 26px; color: #212529; text-align: justify; background-color: #ffffff; font-family: NotoSerif !important; margin: 0px 0px !important 8px 0px !important;\">Về kh&ocirc;ng gian th&igrave; qu&aacute;n ở đ&acirc;y rất rộng, c&oacute; hai tầng chỗ n&agrave;o cũng c&oacute; mặt tiền tho&aacute;ng m&aacute;t, view nh&igrave;n ra b&ecirc;n ngo&agrave;i con phố đẹp, b&igrave;nh y&ecirc;n. Ở dưới tầng 1 th&igrave; kh&ocirc;ng c&oacute; điều h&ograve;a, hầu như l&agrave; kh&ocirc;ng gian mở n&ecirc;n ai hay sử dụng thuốc l&aacute; hay vape c&oacute; thể ngồi ở đ&acirc;y.</p>\r\n<p style=\"box-sizing: border-box; text-rendering: optimizelegibility; font-size: 17px; line-height: 26px; color: #212529; text-align: justify; background-color: #ffffff; font-family: NotoSerif !important; margin: 0px 0px !important 8px 0px !important;\">Tầng hai th&igrave; si&ecirc;u m&aacute;t mẻ, m&ugrave;a h&egrave; qua đ&acirc;y tr&aacute;nh n&oacute;ng th&igrave; phải n&oacute;i l&agrave; chuẩn chỉnh. Tr&ecirc;n tầng n&agrave;y cũng c&oacute; ban c&ocirc;ng rộng, tọa độ sống ảo c&oacute; một kh&ocirc;ng hai của qu&aacute;n, v&agrave;o Laika Cafe Trung H&ograve;a m&agrave; kh&ocirc;ng chụp ở đ&acirc;y th&igrave; qu&aacute; ph&iacute;.&nbsp;</p>\r\n<figure class=\"expNoEdit\" style=\"box-sizing: border-box; text-rendering: optimizelegibility; display: block; margin-top: 0px; margin-bottom: 8px; width: 658px; text-align: center; color: #212529; font-size: 14px; background-color: #ffffff; font-family: NotoSerif !important;\"><img class=\"lazyload loaded\" style=\"box-sizing: border-box; text-rendering: optimizelegibility; object-fit: cover; height: auto !important; vertical-align: middle; border-style: none; width: 658px; max-height: 100%; margin: 0px !important; max-width: 100%;\" src=\"https://media.travelmag.vn/files/mylinh/2020/09/20/laika-cafe-10-2146.jpg\" alt=\"Ảnh: Đỗ Th&ugrave;y Trang\" width=\"680\" height=\"510\" data-src=\"https://media.travelmag.vn/files/mylinh/2020/09/20/laika-cafe-10-2146.jpg\" data-was-processed=\"true\" />\r\n<figcaption style=\"box-sizing: border-box; text-rendering: optimizelegibility; width: 664px; background: #f2f2f2; padding: 3px; margin: 0px auto; font-size: 13px !important;\">\r\n<h2 style=\"box-sizing: border-box; text-rendering: optimizelegibility; margin: 0px; font-weight: normal; line-height: 19px; color: inherit; font-style: italic; padding: 5px 10px; background: #f5f5f5; font-size: 13px !important;\">Ảnh: Đỗ Th&ugrave;y Trang</h2>\r\n</figcaption>\r\n</figure>\r\n<figure class=\"expNoEdit\" style=\"box-sizing: border-box; text-rendering: optimizelegibility; display: block; margin-top: 0px; margin-bottom: 8px; width: 658px; text-align: center; color: #212529; font-size: 14px; background-color: #ffffff; font-family: NotoSerif !important;\"><img class=\"lazyload loaded\" style=\"box-sizing: border-box; text-rendering: optimizelegibility; object-fit: cover; height: auto !important; vertical-align: middle; border-style: none; width: 658px; max-height: 100%; margin: 0px !important; max-width: 100%;\" src=\"https://media.travelmag.vn/files/mylinh/2020/09/20/laika-cafe-7-2144.jpg\" alt=\"Ảnh: Đỗ Th&ugrave;y Trang\" width=\"680\" height=\"510\" data-src=\"https://media.travelmag.vn/files/mylinh/2020/09/20/laika-cafe-7-2144.jpg\" data-was-processed=\"true\" /></figure>', '204018558_986017602201091_7657841524277310198_n.jpg', 18, '2021-11-03 04:40:39', NULL),
(44, 0, 'Laika cafe Hà Đông - Cùng mình khám phá nhé', '<p>Địa chỉ: 58 BT8 Văn Qu&aacute;n, H&agrave; Đ&ocirc;ng.</p>\r\n<p>Địa điểm qu&aacute;n cafe Laika mới n&agrave;y với 4 tầng c&oacute; cả kh&ocirc;ng gian s&acirc;n thượng, ban c&ocirc;ng, qu&aacute;n với kh&ocirc;ng gian rộng r&atilde;i, c&oacute; thể ngồi ngắm hồ Văn Qu&aacute;n.</p>', '<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #4a4d4e; font-family: Quicksand, sans-serif; font-size: 15px; letter-spacing: -0.5px; background-color: #ffffff;\">Chuỗi qu&aacute;n cafe Laika c&oacute; th&ecirc;m chi nh&aacute;nh tại H&agrave; Đ&ocirc;ng. Địa điểm qu&aacute;n cafe Laika mới n&agrave;y với 4 tầng c&oacute; cả kh&ocirc;ng gian s&acirc;n thượng, ban c&ocirc;ng, qu&aacute;n với kh&ocirc;ng gian rộng r&atilde;i, c&oacute; thể ngồi ngắm hồ Văn Qu&aacute;n. Với thiết kế chủ đạo đặc trưng của Laika cafe l&agrave; tone m&agrave;u x&aacute;m trung t&iacute;nh&hellip; Nội thất gỗ mang đặc trưng chuỗi qu&aacute;n cafe Laika nhưng vẫn c&oacute; điểm kh&aacute;c biệt ri&ecirc;ng. Kh&ocirc;ng gian cafe Laika H&agrave; Đ&ocirc;ng rộng tho&aacute;ng, nhiều g&oacute;c checkin v&ocirc; c&ugrave;ng đẹp, ấn tượng. Đặc biệt l&agrave; mặt tiền cao rộng, ngập tr&agrave;n &aacute;nh s&aacute;ng chắc chắn sẽ l&agrave; điểm hẹn cafe l&yacute; tưởng. Tầng 3 qu&aacute;n chủ yếu l&agrave; b&agrave;n lớn, kh&aacute; hợp để họp nh&oacute;m hay b&agrave;n việc đ&ocirc;ng người. Menu đồ uống trong qu&aacute;n cũng kh&aacute; đa dạng, nhiều lựa chọn, gi&aacute; cả trung b&igrave;nh từ 35-65k.</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #4a4d4e; font-family: Quicksand, sans-serif; font-size: 15px; letter-spacing: -0.5px; background-color: #ffffff;\">&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #4a4d4e; font-family: Quicksand, sans-serif; font-size: 15px; letter-spacing: -0.5px; background-color: #ffffff;\"><img class=\"fr-fic fr-dib pswp-img\" style=\"box-sizing: border-box; border: 0px; vertical-align: middle; max-width: 100%; height: auto; width: 750px;\" src=\"https://thicongnhahang.vn/storage/app/media/uploaded-files/laika-ha-dong-05.jpg\" data-result=\"success\" /></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #4a4d4e; font-family: Quicksand, sans-serif; font-size: 15px; letter-spacing: -0.5px; background-color: #ffffff;\">&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #4a4d4e; font-family: Quicksand, sans-serif; font-size: 15px; letter-spacing: -0.5px; background-color: #ffffff;\"><img class=\"fr-fic fr-dib pswp-img\" style=\"box-sizing: border-box; border: 0px; vertical-align: middle; max-width: 100%; height: auto; width: 750px;\" src=\"https://thicongnhahang.vn/storage/app/media/uploaded-files/laika-ha-dong-04.jpg\" data-result=\"success\" /></p>', '203368605_990100111792840_5529883794874289058_n.jpg', 8, '2021-11-03 04:41:09', NULL),
(47, 0, 'Laika cafe Hoàn Kiếm - Quán cafe đầu tiên', '<p>Địa chỉ: số 18 H&agrave;ng C&oacute;t, Ho&agrave;n Kiếm</p>\r\n<p>Laika cafe Ho&agrave;n Kiếm c&oacute; lợi thế nằm tr&ecirc;n khu phố cổ H&agrave; Nội. Qu&aacute; c&agrave; ph&ecirc; với kh&ocirc;ng gian 5 tầng tr&ecirc;n số 18 phố H&agrave;ng C&oacute;t, quận Ho&agrave;n Kiếm, H&agrave; Nội.</p>', '<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #4a4d4e; font-family: Quicksand, sans-serif; font-size: 15px; letter-spacing: -0.5px; background-color: #ffffff;\">Khu phố cổ đ&ocirc;ng đ&uacute;c, đặc trưng bởi những ng&ocirc;i nh&agrave; s&aacute;t nhau đi c&ugrave;ng năm th&aacute;ng. Laika cafe nổi bật trong khu phố khiến ai gh&eacute; qua con đường n&agrave;y đều sẽ bị thu h&uacute;t, ch&uacute; &yacute; v&igrave; sự nổi bật của qu&aacute;n c&agrave; ph&ecirc; Laika. &nbsp;Giữa l&ograve;ng phố cổ H&agrave; Nội xuất hiện h&igrave;nh ảnh của một Hội An cổ k&iacute;nh, thu nhỏ, v&agrave;ng rực nổi bật, lung linh giữa g&oacute;c phố trầm mặc.</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #4a4d4e; font-family: Quicksand, sans-serif; font-size: 15px; letter-spacing: -0.5px; background-color: #ffffff;\">&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #4a4d4e; font-family: Quicksand, sans-serif; font-size: 15px; letter-spacing: -0.5px; background-color: #ffffff;\"><img class=\"fr-fic fr-dib pswp-img\" style=\"box-sizing: border-box; border: 0px; vertical-align: middle; max-width: 100%; height: auto; width: 750px;\" src=\"https://thicongnhahang.vn/storage/app/media/uploaded-files/thiet-ke-quan-cafe-laika-09.jpg\" data-result=\"success\" /></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #4a4d4e; font-family: Quicksand, sans-serif; font-size: 15px; letter-spacing: -0.5px; background-color: #ffffff;\">&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #4a4d4e; font-family: Quicksand, sans-serif; font-size: 15px; letter-spacing: -0.5px; background-color: #ffffff;\">Qu&aacute;n c&agrave; ph&ecirc; Laika tr&ecirc;n phố cổ H&agrave; Nội kh&ocirc;ng chỉ may mắn c&oacute; được một vị tr&iacute; v&agrave; view nh&igrave;n ra phố đẹp; m&agrave; khoảng kh&ocirc;ng gian của qu&aacute;n được thiết kế mang n&eacute;t đặc trưng v&agrave; đầy ấn tượng kh&oacute; qu&ecirc;n với kh&aacute;ch h&agrave;ng. H&igrave;nh ảnh qu&aacute;n cafe 5 tầng nổi bật với tone v&agrave;ng đan xen c&ugrave;ng m&agrave;u nắng v&agrave; những ng&ocirc;i nh&agrave; cổ mang ch&uacute;t ho&agrave;i niệm, nhẹ nh&agrave;ng ẩn m&igrave;nh trong l&ograve;ng phố. Khi đ&ecirc;m về qu&aacute;n trở n&ecirc;n thu h&uacute;t, lung linh, huyền ảo hơn nhờ những &aacute;nh đ&egrave;n đặc trưng của khu phố cổ Hội An giữa l&ograve;ng H&agrave; Nội. Khung cảnh, &aacute;nh s&aacute;ng mộng mơ của đ&egrave;n lồng đa m&agrave;u sắc h&ograve;a c&ugrave;ng vẻ đẹp nhẹ nh&agrave;ng của phố cổ sẽ khiến bạn li&ecirc;n tưởng đến kh&ocirc;ng gian Hội An thu nhỏ. Bạn c&ograve;n c&oacute; thể quan s&aacute;t được cả đo&agrave;n t&agrave;u chạy qua, ru v&agrave;o tai những &acirc;m thanh thường nhật của cuộc sống, vừa gấp g&aacute;p lại vừa lặng lẽ. Đồ uống gi&aacute; th&agrave;nh ở qu&aacute;n kh&aacute; phải chăng so với mặt bằng chung, hương vị ph&ugrave; hợp với đa số mọi người.&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #4a4d4e; font-family: Quicksand, sans-serif; font-size: 15px; letter-spacing: -0.5px; background-color: #ffffff;\">&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #4a4d4e; font-family: Quicksand, sans-serif; font-size: 15px; letter-spacing: -0.5px; background-color: #ffffff;\"><img class=\"fr-fic fr-dib pswp-img\" style=\"box-sizing: border-box; border: 0px; vertical-align: middle; max-width: 100%; height: auto; width: 750px;\" src=\"https://thicongnhahang.vn/storage/app/media/uploaded-files/thiet-ke-quan-cafe-laika-011.jpg\" data-result=\"success\" /></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #4a4d4e; font-family: Quicksand, sans-serif; font-size: 15px; letter-spacing: -0.5px; background-color: #ffffff;\">Tr&ecirc;n đ&acirc;y l&agrave; một số &nbsp;thiết kế qu&aacute;n cafe Laika ấn tượng tại H&agrave; Nội, c&aacute;c chủ đầu tư c&oacute; &yacute; định kinh doanh qu&aacute;n cafe c&oacute; thể tham khảo hong c&aacute;ch qu&aacute;n cafe Laika. Ngo&agrave;i ra, để c&oacute; một thiết kế thực sự th&ocirc;ng minh, khoa học th&igrave; việc lựa chọn một đơn vị thiết kế chuy&ecirc;n nghiệp l&agrave; việc cần c&acirc;n nhắc. KenDesign l&agrave; đơn vị tư vấn/ thiết kế với nhiều năm kinh nghiệm trong lĩnh vực n&agrave;y; chắc chắn sẽ đem đến cho bạn những giải ph&aacute;p ph&ugrave; hợp với thực tế c&ocirc;ng tr&igrave;nh, gi&uacute;p bạn biến &yacute; tưởng th&agrave;nh hiện thực.</p>', '241293060_1040028523466665_8397315372505624349_n.jpg', 2, '2021-10-29 05:41:25', NULL),
(49, 0, 'Laika cafe Đường Láng', '<p>Điạ chỉ: 64 Đường L&aacute;ng</p>\r\n<p>Chi nh&aacute;nh mới của Laika ở đường L&aacute;ng với quy m&ocirc; v&agrave; sự đầu tư lớn. Kh&ocirc;ng gian qu&aacute;n ở đ&acirc;y rộng r&atilde;i đủ sức đ&aacute;p ứng cả nhu cầu l&agrave;m việc lẫn sống ảo, đ&acirc;y l&agrave; điểm cộng lớn nhất thu h&uacute;t thực kh&aacute;ch đến cafe Laika ng&agrave;y c&agrave;ng nhiều hơn.</p>', '<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #4a4d4e; font-family: Quicksand, sans-serif; font-size: 15px; letter-spacing: -0.5px; background-color: #ffffff;\">Qu&aacute;n cafe Laika đường L&aacute;ng với 2 tầng rộng rãi và nhiều chỗ ngồi. Tầng 1 với nội thất chủ yếu là bàn thấp, tầng 2 nhiều bàn cao hơn. Thiết kế giữa các gian được thông nhau v&ugrave;a c&oacute; kh&ocirc;ng gian ri&ecirc;ng vừa tạo độ thoáng và rộng rãi. Bàn ghế trong qu&aacute;n được bố trí phù hợp cho cả việc học lẫn tụ tập bạn bè cuối tuần.</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #4a4d4e; font-family: Quicksand, sans-serif; font-size: 15px; letter-spacing: -0.5px; background-color: #ffffff;\">Tầng 1 hợp ngồi n&oacute;i chuyện, tâm sự, đến với tầng 2 là nơi đúng chuẩn làm việc và d&agrave;nh cho bạn th&iacute;ch ngắm phố phường, d&ograve;ng xe qua lại với g&oacute;c ban c&ocirc;ng cực k&igrave; ấn tượng, thu h&uacute;t.</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #4a4d4e; font-family: Quicksand, sans-serif; font-size: 15px; letter-spacing: -0.5px; background-color: #ffffff;\">&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #4a4d4e; font-family: Quicksand, sans-serif; font-size: 15px; letter-spacing: -0.5px; background-color: #ffffff;\"><img class=\"fr-fic fr-dib pswp-img\" style=\"box-sizing: border-box; border: 0px; vertical-align: middle; max-width: 100%; height: auto; width: 750px;\" src=\"https://thicongnhahang.vn/storage/app/media/uploaded-files/laika-lang-02.jpg\" data-result=\"success\" /></p>', '183692747_961959354606916_7498582999681151789_n.jpg', 4, '2021-10-30 01:53:56', NULL),
(50, 0, 'LAIKA TUYỂN DỤNG ✏✏ CHÚNG MÌNH CẦN CÁC BẠN', '<p>Ch&uacute;ng m&igrave;nh cần c&aacute;c bạn ở c&aacute;c vị tr&iacute;:</p>\r\n<p>▪ Quản l&yacute; (c&oacute; kinh nghiệm )</p>\r\n<p>▪ Nh&acirc;n vi&ecirc;n phục vụ</p>\r\n<p>▪ Nh&acirc;n vi&ecirc;n pha chế</p>\r\n<p>APPLY NGAY N&Agrave;O !!!</p>', '<p>LAIKA Cafe vẫn đang t&igrave;m những bạn trẻ năng động c&ugrave;ng ch&uacute;ng m&igrave;nh c&oacute; những giờ l&agrave;m việc thật vui v&agrave; kiếm được thật nhiều \"x&egrave;ng\" sau gi&atilde;n c&aacute;ch. C&aacute;c bạn t&igrave;m hiểu chi tiết c&ocirc;ng việc dưới đ&acirc;y nha!</p>\r\n<p>✏ VỊ TR&Iacute;</p>\r\n<p>- Quản l&yacute; (y&ecirc;u cầu c&oacute; kinh nghiệm )</p>\r\n<p>- Nh&acirc;n vi&ecirc;n phục vụ</p>\r\n<p>- Nh&acirc;n vi&ecirc;n pha chế</p>\r\n<p>✏ Y&Ecirc;U CẦU</p>\r\n<p>- Đủ 18 tuổi.</p>\r\n<p>- Nhanh nhẹn, sạch sẽ, c&oacute; &yacute; thức, tr&aacute;ch nhiệm với c&ocirc;ng việc, biết giao tiếp.</p>\r\n<p>- Trung thực, chăm chỉ v&agrave; tận t&acirc;m.</p>\r\n<p>- Y&ecirc;u th&iacute;ch c&ocirc;ng việc, ưu ti&ecirc;n những bạn n&agrave;o c&oacute; &yacute; định đồng h&agrave;nh l&acirc;u d&agrave;i với Laika.</p>\r\n<p>✏ QUYỀN LỢI</p>\r\n<p>- Lương thưởng theo năng lực (lương cứng + thưởng).</p>\r\n<p>- Thưởng ng&agrave;y lễ tết, sinh nhật.</p>\r\n<p>- Được đ&agrave;o tạo kỹ năng phục vụ, pha chế.</p>\r\n<p>- M&ocirc;i trường trẻ trung, năng động, sếp vui t&iacute;nh đồng nghiệp th&acirc;n thiện.&nbsp;</p>\r\n<p>- Cơ hội thăng tiến rất hấp dẫn.</p>\r\n<p>✏ CHẾ ĐỘ</p>\r\n<p>Lương Khởi Điểm:</p>\r\n<p>- Vị tr&iacute; quản l&yacute; : Lương thỏa thuận theo năng lực từ 8.000.000 - 12.000.000 VNĐ</p>\r\n<p>- Vị tr&iacute; (phục vụ pha chế) thỏa thuận theo năng lực từ 17.000 - 22.000 VNĐ</p>\r\n<p>✏ THỜI GIAN L&Agrave;M VIỆC</p>\r\n<p>- Ca s&aacute;ng: 7h - 15h</p>\r\n<p>- Ca chiều: 15h - 23h</p>\r\n<p>- Part-time: 18h - 23h</p>\r\n<p>✏ ĐỊA ĐIỂM L&Agrave;M VIỆC | H&Agrave; NỘI</p>\r\n<p>- 65 Trần Nh&acirc;n T&ocirc;ng, Hai B&agrave; Trưng.</p>\r\n<p>- 1 L&yacute; Th&aacute;i Tổ, Ho&agrave;n Kiếm.</p>\r\n<p>- 40 B&agrave; Triệu, Ho&agrave;n Kiếm.</p>\r\n<p>- 108-109 K2 ng&otilde; 48 Tạ Quang Bửu.</p>\r\n<p>- 91 Trung Ho&agrave;.</p>\r\n<p>- 64 Đường L&aacute;ng.</p>\r\n<p>- 58-BT8 Văn Qu&aacute;n, H&agrave; Đ&ocirc;ng.</p>\r\n<p>- 151 Tr&iacute;ch S&agrave;i , T&acirc;y Hồ.</p>\r\n<p>- 25 L&ecirc; Văn Thi&ecirc;m, Thanh Xu&acirc;n.</p>\r\n<p>- 34 Thợ Nhuộm, Ho&agrave;n Kiếm.</p>\r\n<p>- 8 Phan Chu Trinh, Ho&agrave;n Kiếm.</p>\r\n<p>- A21-HH6 đường Ng&ocirc; Viết Thụ, KĐT Việt Hưng, Long Bi&ecirc;n.</p>\r\n<p>- A33 Ho&agrave;ng Ng&acirc;n, Cầu Giấy.</p>\r\n<p>- To&agrave; B5 Trần Đăng Ninh, Cầu Giấy.</p>\r\n<p>- 17 - BT1 Bắc Linh Đ&agrave;m.</p>\r\n<p>?? ỨNG TUYỂN TẠI: https://bit.ly/laikarecruitment2021</p>\r\n<p>☎ Mọi thắc mắc vui l&ograve;ng li&ecirc;n hệ Ms.Tr&acirc;m Anh - 0912140896.</p>\r\n<p>Hẹn gặp c&aacute;c bạn tại LAIKA!</p>\r\n<p>━━━━━━━━━━━━━━&nbsp;</p>\r\n<p>Hotline: 096 696 6886</p>\r\n<p>Zalo: https://bit.ly/laikacafezalo</p>\r\n<p>IG: http://bit.ly/laikacafeinstagram</p>\r\n<p>Liên hệ nhượng quyền trực tiếp vào Hotline hoặc Fanpage</p>', '244862397_1062517057884478_8451426759298158188_n.jpg', 8, '2021-10-30 00:55:21', NULL),
(52, 0, 'Laika Cafe - Từ những bước đầu xây dựng thương hiệu đến chuỗi cafe sống ảo cực Hot trong giới trẻ.', '<p>Nổi tiếng l&agrave; một trong những chuỗi cafe đường t&agrave;u hot nhất với giới trẻ, &iacute;t ai biết, Laika cũng từng chật vật đi t&igrave;m chỗ đứng. Tuy nhi&ecirc;n, với giải ph&aacute;p marketing đ&uacute;ng đắn, qu&aacute;n Cafe &iacute;t ai biết ng&agrave;y n&agrave;o đ&atilde; trở th&agrave;nh điểm đến cực thu h&uacute;t.&nbsp;</p>', '<p>I. Đề b&agrave;i:&nbsp;</p>\r\n<p>- Laika l&agrave; một qu&aacute;n cafe ra đời v&agrave;o th&aacute;ng 6/2019 tại H&agrave;ng C&oacute;t.</p>\r\n<p>- Laika chưa x&acirc;y dựng định vị thương hiệu ch&iacute;nh x&aacute;c.</p>\r\n<p>&rarr; Laika muốn t&igrave;m một đơn vị marketing chuy&ecirc;n nghiệp, giải quyết b&agrave;i to&aacute;n l&agrave;m thế n&agrave;o để Laika trở n&ecirc;n kh&aacute;c biệt? L&agrave;m thế n&agrave;o để kh&aacute;ch h&agrave;ng k&eacute;o đến, tăng doanh thu cho qu&aacute;n?</p>\r\n<p>II. Nghi&ecirc;n cứu:</p>\r\n<p>- Thương hiệu + Đối thủ: Carrot thực hiện nghi&ecirc;n cứu v&agrave; v&agrave; khảo s&aacute;t trong 2 tuần để đưa ra điểm mạnh, điểm yếu, cơ hội, th&aacute;ch thức của Laika. Từ đ&oacute;, t&igrave;m được USP của qu&aacute;n: Laika Cafe - Cafe săn t&agrave;u với điểm check-in đường t&agrave;u độc - đỉnh.</p>\r\n<p>- Kh&aacute;ch h&agrave;ng:&nbsp;</p>\r\n<p>+ Những bạn trẻ, trong độ tuổi từ 18 - 24.</p>\r\n<p>+ Insight: th&iacute;ch kh&ocirc;ng gian tho&aacute;ng m&aacute;t, view đẹp, hay tụ tập bạn b&egrave; v&agrave; muốn t&igrave;m những nơi thoải m&aacute;i để c&oacute; thể l&agrave; ch&iacute;nh m&igrave;nh.</p>\r\n<p>III. Chiến lược:</p>\r\n<p>Sau khi nghi&ecirc;n cứu, Carrot quyết định đề xuất x&acirc;y dựng Laika:</p>\r\n<p>- Định vị: Cafe săn t&agrave;u - Nơi mọi người c&oacute; thể thoải m&aacute;i, l&agrave; ch&iacute;nh m&igrave;nh (Laika Cafe - Be yourself).</p>\r\n<p>- T&iacute;nh c&aacute;ch thương hiệu: Một ch&agrave;ng trai/ c&ocirc; g&aacute;i sống ph&oacute;ng kho&aacute;ng, t&iacute;nh c&aacute;ch kh&aacute;c biệt, mộc mạc, giản dị, kh&ocirc;ng quan t&acirc;m đến người kh&aacute;c đ&aacute;nh gi&aacute; thế n&agrave;o về m&igrave;nh, kh&ocirc;ng c&acirc;u nệ, kh&ocirc;ng th&iacute;ch n&oacute;i nhiều.</p>\r\n<p>- &Yacute; tưởng lớn: Ở Laika, ch&uacute;ng t&ocirc;i kh&ocirc;ng chỉ b&aacute;n cafe, kh&ocirc;ng chỉ quan t&acirc;m đến l&atilde;i m&agrave; ch&uacute;ng t&ocirc;i b&aacute;n trải nghiệm, đem lại niềm vui cho mọi người. Kh&aacute;ch h&agrave;ng sẽ &ldquo;L&atilde;i cả&rdquo;.</p>', '185878707_961958607940324_646943568832363965_n.jpg', 3, '2021-10-29 23:29:08', NULL),
(53, 0, 'CÀ PHÊ ĐIIIII!', '<p>M&igrave;nh đi c&agrave; ph&ecirc; h&ocirc;ng?</p>', '<p>B&acirc;y giờ l&agrave; 19h58, theo l&yacute; thuyết vẫn chưa phải l&agrave; giờ thực sự \"hot\" để đăng b&agrave;i. Nhưng c&ograve;n g&igrave; \"hot\" hơn tin MỞ CỬA H&Agrave;NG ĂN, UỐNG cơ chứ. N&ecirc;n l&agrave;, sau cuộc họp nhanh, ch&uacute;ng m&igrave;nh xin tr&acirc;n trọng th&ocirc;ng b&aacute;o:</p>\r\n<p>⏰⏰ To&agrave;n bộ hệ thống LAIKA Cafe sẽ mở cửa v&agrave;o ng&agrave;y 14... &agrave; kh&ocirc;ng,</p>\r\n<p>Tuy \"chậm ch&acirc;n\" hơn c&aacute;c bạn \"c&ugrave;ng trang lứa\", nhưng ch&uacute;ng m&igrave;nh mong muốn chuẩn bị cẩn thận nhất từ đồ uống đến kh&ocirc;ng gian để đ&oacute;n tiếp c&aacute;c bạn thật \"xịn\" sau thời gian d&agrave;i gi&atilde;n c&aacute;ch.</p>\r\n<p>Vui chơi kh&ocirc;ng qu&ecirc;n nhiệm vụ, ch&uacute;ng m&igrave;nh vẫn c&ugrave;ng nhau duy tr&igrave; việc rửa tay, đeo khẩu trang v&agrave; giữ khoảng c&aacute;ch để bảo vệ sức khoẻ ch&iacute;nh m&igrave;nh v&agrave; mọi người c&aacute;c bạn nh&eacute;.</p>\r\n<p>Ch&uacute;ng m&igrave;nh phải đi dọn dẹp, chuẩn bị đ&acirc;y, c&aacute;c bạn nhớ 15/10 n&agrave;y đến LAIKA c&agrave; ph&ecirc; đ&oacute; nha!</p>\r\n<p>Hẹn gặp c&aacute;c bạn!</p>\r\n<p>━━━━━━━━━━━━━━</p>\r\n<p>▪ LAIKA Cafe - 91 Trung Hoà - Hà Nội</p>\r\n<p>▪ LAIKA Cafe - 64 Đường Láng - Hà Nội</p>\r\n<p>▪ LAIKA Cafe - 5 C3 Làng Quốc tế Thăng Long - Cầu Giấy - Hà Nội</p>\r\n<p>▪ LAIKA Cafe - 52 Lĩnh Nam - Hoàng Mai - Hà Nội</p>\r\n<p>▪ LAIKA Cafe - 58-BT8 Khu đô thị Văn Quán - Hà Đông - Hà Nội</p>\r\n<p>▪ LAIKA Cafe - 229 Phố Huế - Hai Bà Trưng - Hà Nội</p>\r\n<p>▪ LAIKA Cafe - 108 -109 ngõ 48 Tạ Quang Bửu - Hà Nội</p>\r\n<p>▪ LAIKA Cafe - Đại học Hà Nội - 9 Nguyễn Tr&atilde;i - H&agrave; Nội</p>\r\n<p>▪ LAIKA Cafe - 8 Phan Chu Trinh, Ho&agrave;n Kiếm</p>\r\n<p>▪ LAIKA Cafe - 112 Lê Hồng Phong - Thành phố Vinh</p>\r\n<p>▪ LAIKA Cafe - 411 Hạ Long - Quảng Ninh</p>\r\n<p>▪ LAIKA Cafe - 102 Quang Trung, Tuy&ecirc;n Quang</p>\r\n<p>▪ LAIKA Cafe - A33 Ho&agrave;ng Ng&acirc;n, Cầu Giấy, H&agrave; Nội</p>\r\n<p>▪ LAIKA Cafe - To&agrave; B5, Trần Đăng Ninh, Cầu Giấy, Hà Nội</p>\r\n<p>▪ LAIKA Cafe - 17, BT1, Bắc Linh Đ&agrave;m</p>\r\n<p>▪ LAIKA Cafe - 151 Tr&iacute;ch S&agrave;i , T&acirc;y Hồ</p>\r\n<p>▪ LAIKA Cafe - 35 L&ecirc; Văn Thi&ecirc;m, Thanh Xu&acirc;n</p>\r\n<p>▪ LAIKA Cafe - 34 Thợ Nhuộm, Ho&agrave;n Kiếm</p>\r\n<p>▪ LAIKA Cafe - A21-HH6 đường Ng&ocirc; Viết Thụ, KĐT Việt Hưng, Long Bi&ecirc;n</p>\r\n<p>☎️ Hotline: 096 696 6886</p>\r\n<p>Zalo: https://bit.ly/laikacafezalo</p>\r\n<p>IG: http://bit.ly/laikacafeinstagram</p>\r\n<p>Liên hệ nhượng quyền trực tiếp vào Hotline hoặc Fanpage</p>', '150507603_905970613539124_1919186705727844164_n.jpg', 4, '2021-10-30 03:15:03', NULL),
(54, 0, 'Wake LAIKA up when Covid ends!', '<p>Laika miss you !!!</p>', '<p>C&oacute; lẽ, từ trưa nay, newsfeed của bạn đ&atilde; ngập tr&agrave;n th&ocirc;ng tin chia sẻ về việc \"Đ&oacute;ng cửa h&agrave;ng qu&aacute;n từ 13/7\". Chưa kịp gặp nhau đủ l&acirc;u th&igrave; thật buồn, ch&uacute;ng ta lại phải n&oacute;i lời tạm biệt.</p>\r\n<p>Tạm xa v&agrave;i ng&agrave;y để được b&ecirc;n nhau d&agrave;i l&acirc;u. Ch&uacute;ng m&igrave;nh h&atilde;y c&ugrave;ng nhau giữ g&igrave;n sức khoẻ thật tốt, luyện tập kỹ năng mới, t&igrave;m kiếm th&ecirc;m sở th&iacute;ch, nu&ocirc;i dưỡng đam m&ecirc;, ph&aacute;t triển t&acirc;m hồn chờ ng&agrave;y dịch tan, ta lại \"bung lụa\" nha!</p>\r\n<p>Hẹn sớm gặp lại bạn!</p>\r\n<p>━━━━━━━━━━━━━━</p>\r\n<p>▪ LAIKA Cafe - 91 Trung Hoà - Hà Nội</p>\r\n<p>▪ LAIKA Cafe - 64 Đường Láng - Hà Nội</p>\r\n<p>▪ LAIKA Cafe - 5 C3 Làng Quốc tế Thăng Long - Cầu Giấy - Hà Nội</p>\r\n<p>▪ LAIKA Cafe - 52 Lĩnh Nam - Hoàng Mai - Hà Nội</p>\r\n<p>▪ LAIKA Cafe - 58-BT8 Khu đô thị Văn Quán - Hà Đông - Hà Nội</p>\r\n<p>▪ LAIKA Cafe - 229 Phố Huế - Hai Bà Trưng - Hà Nội</p>\r\n<p>▪ LAIKA Cafe - 108 -109 ngõ 48 Tạ Quang Bửu - Hà Nội</p>\r\n<p>▪ LAIKA Cafe - Đại học Hà Nội - 9 Nguyễn Tr&atilde;i - H&agrave; Nội</p>\r\n<p>▪ LAIKA Cafe - 8 Phan Chu Trinh, Ho&agrave;n Kiếm</p>\r\n<p>▪ LAIKA Cafe - 112 Lê Hồng Phong - Thành phố Vinh</p>\r\n<p>▪ LAIKA Cafe - 411 Hạ Long - Quảng Ninh</p>\r\n<p>?????? ????</p>\r\n<p>▪ LAIKA Cafe - A33 Ho&agrave;ng Ng&acirc;n, Cầu Giấy, H&agrave; Nội&nbsp;</p>\r\n<p>▪ LAIKA Cafe - To&agrave; B5, Trần Đăng Ninh, Cầu Giấy, Hà Nội</p>\r\n<p>▪ LAIKA Cafe - 17, BT1, Bắc Linh Đ&agrave;m&nbsp;</p>\r\n<p>▪ LAIKA Cafe - 151 Tr&iacute;ch S&agrave;i , T&acirc;y Hồ</p>\r\n<p>▪ LAIKA Cafe - 35 L&ecirc; Văn Thi&ecirc;m, Thanh Xu&acirc;n</p>\r\n<p>▪ LAIKA Cafe - 34 Thợ Nhuộm, Ho&agrave;n Kiếm</p>\r\n<p>▪ LAIKA Cafe - 102 Quang Trung, Tuy&ecirc;n Quan</p>\r\n<p>▪ LAIKA Cafe - A21-HH6 đường Ng&ocirc; Viết Thụ, KĐT Việt Hưng, Long Bi&ecirc;n</p>', '162191168_927102368092615_3524292228326942183_n.jpg', 6, '2021-10-30 03:25:23', NULL),
(55, 0, 'Laika’s Playlist - Cho cuối tuần thật “chill” 123', '<p>Nghe nhạc c&ugrave;ng m&igrave;nh nha !</p>', '<p>Thức dậy, tỉnh t&aacute;o bằng &ldquo;Love Myself - Hailee Steinfeld&rdquo;, với một th&ocirc;ng điệp đơn giản nhưng cực k&igrave; s&acirc;u sắc &ldquo;Y&ecirc;u bản th&acirc;n m&igrave;nh trước đ&atilde;, đừng để cảm x&uacute;c bị chi phối bởi người kh&aacute;c&rdquo;, để cả ng&agrave;y đầy năng lượng.</p>\r\n<p>Nghỉ giữa buổi, nhẹ nh&agrave;ng nhưng thổn thức c&ugrave;ng &ldquo;C&oacute; ch&agrave;ng trai viết l&ecirc;n c&acirc;y - Phan Mạnh Quỳnh&rdquo;, một bản ballad s&acirc;u lắng n&oacute;i về chuyện t&igrave;nh dang dở giữa Ngạn v&agrave; H&agrave; Lan, sẽ k&eacute;o &ldquo;mood&rdquo; của bạn xuống một ch&uacute;t.</p>\r\n<p>Kết ng&agrave;y bằng một b&agrave;i h&aacute;t vui tươi năng động &ldquo;Thở - Dalab ft Juky San&rdquo; truyền tải một th&ocirc;ng điệp rất t&iacute;ch cực &ldquo;H&atilde;y tr&acirc;n trọng hiện tại v&agrave; cuộc sống của m&igrave;nh, tạm g&aacute;c những bộn bề của cuộc sống, cho bản th&acirc;n được nghỉ ngơi, sống chậm lại để cảm nhận cuộc sống, bạn nh&eacute;&rdquo;.</p>\r\n<p>━━━━━━━━━━━━━━</p>', '172087327_942919176510934_2291086709838391717_n.png', 13, '2021-10-30 04:40:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tuyen_dung`
--

CREATE TABLE `tbl_tuyen_dung` (
  `id_tuyen_dung` int(11) NOT NULL,
  `id_chi_nhanh` int(11) NOT NULL,
  `ten` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `email_dk` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `dia_chi_dk` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `bo_phan_dk` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ca_lam` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ly_do` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_tuyen_dung`
--

INSERT INTO `tbl_tuyen_dung` (`id_tuyen_dung`, `id_chi_nhanh`, `ten`, `email_dk`, `dia_chi_dk`, `bo_phan_dk`, `ca_lam`, `ly_do`) VALUES
(11, 0, 'Nguyễn Hồng Hạnh', 'hanhnh@gmail.com', '12 Đội Cấn, Ba Đình, Hà Nội', 'Quản lý', 'Ca sáng: 7h - 15h', 'Mình đã có khá là nhiều kinh nghiệm ở việc quản lý và mình tự tin sẽ làm tốt công việc này.'),
(12, 0, 'Phan Trung Đức', 'trungducphann@gmail.com', '68 Nguyễn Trãi, Thanh Xuân, Hà Nội', 'Nhân viên pha chế', 'Ca chiều: 15h - 23h', 'Mình rất là yêu thích bộ môn pha chế này.'),
(13, 0, 'Cao Thái Trang', 'caotrang@gmail.com', '7 Phạm Hùng, Thanh Xuân, Hà Nội', 'Nhân viên bảo vệ', 'Part-time: 18h - 23h', 'Tôi còn trẻ và khoẻ.'),
(15, 0, 'Mai Ngọc', 'mn@gmail.com', '12 Chùa Bộc, Đống Đa, Hà Nội', 'Nhân viên phục vụ', 'Part-time: 18h - 23h', 'Mình muốn có thêm thu nhập cho bản thân'),
(16, 0, 'thu thảo', 'dtt@gmail.com', '1 trường chinh,nam định', 'Quản lý', 'Ca sáng: 7h - 15h', 'hihi'),
(17, 0, 'Phạm Thị Hồng', 'hongpham@gmail.com', '23 Cát Bà, Nam Từ Liêm, Hà Nội', 'Quản lý', 'Ca chiều: 15h - 23h', 'Mình đã có khá là nhiều kinh nghiệm ở việc quản lý và mình tự tin sẽ làm tốt công việc này.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_chi_nhanh`
--
ALTER TABLE `tbl_chi_nhanh`
  ADD PRIMARY KEY (`id_chi_nhanh`);

--
-- Indexes for table `tbl_dat_hang`
--
ALTER TABLE `tbl_dat_hang`
  ADD PRIMARY KEY (`id_dat_hang`),
  ADD KEY `id_san_pham` (`id_san_pham`),
  ADD KEY `id_chi_nhanh` (`id_chi_nhanh`);

--
-- Indexes for table `tbl_nguoi_dung`
--
ALTER TABLE `tbl_nguoi_dung`
  ADD PRIMARY KEY (`id_nguoi_dung`),
  ADD KEY `id_chi_nhanh` (`id_chi_nhanh`);

--
-- Indexes for table `tbl_nhan_vien`
--
ALTER TABLE `tbl_nhan_vien`
  ADD PRIMARY KEY (`id_nhan_vien`),
  ADD KEY `id_chi_nhanh` (`id_chi_nhanh`);

--
-- Indexes for table `tbl_phan_hoi`
--
ALTER TABLE `tbl_phan_hoi`
  ADD PRIMARY KEY (`id_phan_hoi`),
  ADD KEY `index` (`id_nhan_vien`);

--
-- Indexes for table `tbl_san_pham`
--
ALTER TABLE `tbl_san_pham`
  ADD PRIMARY KEY (`id_san_pham`);

--
-- Indexes for table `tbl_tin_tuc`
--
ALTER TABLE `tbl_tin_tuc`
  ADD PRIMARY KEY (`id_tin_tuc`),
  ADD KEY `id_chi_nhanh` (`id_chi_nhanh`);

--
-- Indexes for table `tbl_tuyen_dung`
--
ALTER TABLE `tbl_tuyen_dung`
  ADD PRIMARY KEY (`id_tuyen_dung`),
  ADD KEY `id_chi_nhanh` (`id_chi_nhanh`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_chi_nhanh`
--
ALTER TABLE `tbl_chi_nhanh`
  MODIFY `id_chi_nhanh` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tbl_dat_hang`
--
ALTER TABLE `tbl_dat_hang`
  MODIFY `id_dat_hang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `tbl_nguoi_dung`
--
ALTER TABLE `tbl_nguoi_dung`
  MODIFY `id_nguoi_dung` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_nhan_vien`
--
ALTER TABLE `tbl_nhan_vien`
  MODIFY `id_nhan_vien` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_phan_hoi`
--
ALTER TABLE `tbl_phan_hoi`
  MODIFY `id_phan_hoi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `tbl_san_pham`
--
ALTER TABLE `tbl_san_pham`
  MODIFY `id_san_pham` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tbl_tin_tuc`
--
ALTER TABLE `tbl_tin_tuc`
  MODIFY `id_tin_tuc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `tbl_tuyen_dung`
--
ALTER TABLE `tbl_tuyen_dung`
  MODIFY `id_tuyen_dung` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
