<!DOCTYPE html>
<html lang="en"><!-- Basic -->
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">   
   
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
 
     <!-- Site Metas -->
    <title>Laika Cafe | Giới thiệu</title>  
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="images/laikalogo.png" type="image/png">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">    
	<!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css">    
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">


</head>

<body>
	<!-- Start header -->
	<header class="top-navbar">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="container">
				<a class="navbar-brand" href="trang_chu.php">
					<img src="images/abc.png" alt="" height="65.px" width="145.px" />
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars-rs-food" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbars-rs-food">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item"><a class="nav-link" href="trang_chu.php">Trang chủ</a></li>
						<li class="nav-item"><a class="nav-link" href="menu.php">Menu</a></li>
						<li class="nav-item active"><a class="nav-link" href="gioi_thieu.php">Về chúng tôi</a></li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="dropdown-a" data-toggle="dropdown">Trang khác</a>
							<div class="dropdown-menu" aria-labelledby="dropdown-a">
								<a class="dropdown-item" href="dat_hang.php">Đặt hàng</a>
								<a class="dropdown-item" href="nhan_vien.php">Đội ngũ LAIKA</a>
								<a class="dropdown-item" href="chi_nhanh.php">Chi nhánh</a>
								<a class="dropdown-item" href="tuyen_dung.php">Tuyển dụng</a>
							</div>
						</li>
						<li class="nav-item"><a class="nav-link" href="tin_tuc.php">Tin tức</a></li>
						<li class="nav-item"><a class="nav-link" href="lien_he.php">Liên hệ</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<!-- End header -->
	
	<!-- Start header -->
	<div class="all-page-title page-breadcrumb">
		<div class="container text-center">
			<div class="row">
				<div class="col-lg-12">
					<h1>Về chúng tôi</h1>
				</div>
			</div>
		</div>
	</div>
	<!-- End header -->
	
	<!-- Start About -->
	<div class="about-section-box">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6">
					<img src="images/laika-cafe-10-2146.jpg" alt="" class="img-fluid">
				</div>
				<div class="col-lg-6 col-md-6 text-center">
					<div class="inner-column">
						<h1>Welcome To <span>Laika Cafe</span></h1>
						
						<p><span>Laika Cafe</span> sử dụng tone màu xám gỗ để tạo nên sự hài hòa trong nhiều phong cách và không gian. Hiện đại có, nhẹ nhàng có, sang chảnh có, kiểu teen teen theo xu thế cũng có. Không gian của <span>Laika Cafe</span> rất rộng, có hai tầng chỗ nào cũng có mặt tiền thoáng mát, view nhìn ra bên ngoài con phố đẹp, bình yên.</p>
						<p>Ở dưới tầng 1 hầu như là không gian mở nên ai hay sử dụng thuốc lá thì có thể ngồi ở đây. Tầng hai thì siêu mát mẻ. Hệ thống đèn và nội thất góp phần tô điểm thêm vẻ đẹp độc đáo của quán, biến <span>Laika Cafe</span> trở thành điểm đến lý tưởng cho tổ chức sự kiện.</p>
					</div>
				</div>
				<div class="col-md-12">
					<div class="inner-pt">
						<p>Tầng 1 tại <span>Laika Cafe</span> thích hợp để tổ chức các sự kiện giải trí, họp lớp, sinh nhật, hội thảo, đào tạo, workshop, làm việc nhóm, minishow, liveshow,...Được trang bị đầy đủ thiết bị hỗ trợ như máy chiếu, loa, mic,... cùng đội ngũ nhân viên trẻ trung, năng động, luôn tận tình và chu đáo dễ dàng lấy thiện cảm của khách hàng khi đến trải nghiệm.</p>
						<p>Đồ uống ở <span>Laika Cafe</span> rất đa dạng, giá cả phù hợp, nhiều đồ uống dinh dưỡng dành cho các khách hàng quan tâm tới sức khỏe như trà, detox, sinh tố,... Bên ngoài hình thức đồ uống đẹp mắt, rất chỉnh chu trước khi mang ra cho khách.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End About -->
	
	<!-- Start Menu -->
		<div class="gallery-box">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="heading-title text-center">
						<h2>Danh sách đồ uống đa dạng của LAIKA</h2>
					</div>
				</div>
			</div>
			<div class="tz-gallery">
				<div class="row">
					<div class="col-sm-12 col-md-4 col-lg-4">
						<a class="lightbox" href="images/gt1.jpg">
							<img class="img-fluid" src="images/gt1.jpg" alt="Gallery Images">
						</a>
					</div>
					<div class="col-sm-6 col-md-4 col-lg-4">
						<a class="lightbox" href="images/gt2.jpg">
							<img class="img-fluid" src="images/gt2.jpg" alt="Gallery Images">
						</a>
					</div>
					<div class="col-sm-6 col-md-4 col-lg-4">
						<a class="lightbox" href="images/gt3.jpg">
							<img class="img-fluid" src="images/gt3.jpg" alt="Gallery Images">
						</a>
					</div>
					<div class="col-sm-6 col-md-4 col-lg-4">
						<a class="lightbox" href="images/gt4.jpg">
							<img class="img-fluid" src="images/gt4.jpg" alt="Gallery Images">
						</a>
					</div>
					<div class="col-sm-6 col-md-4 col-lg-4">
						<a class="lightbox" href="images/gt5.jpg">
							<img class="img-fluid" src="images/gt5.jpg" alt="Gallery Images">
						</a>
					</div> 
					<div class="col-sm-6 col-md-4 col-lg-4">
						<a class="lightbox" href="images/gt6.jpg">
							<img class="img-fluid" src="images/gt6.jpg" alt="Gallery Images">
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Menu -->
	
	
	
	<!-- Start thông tin liên hệ -->
	<div class="contact-imfo-box">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<i class="fa fa-volume-control-phone"></i>
					<div class="overflow-hidden">
						<h4>Di động</h4>
						<p class="lead">
							+84 941-050-819
						</p>
					</div>
				</div>
				<div class="col-md-4">
					<i class="fa fa-envelope"></i>
					<div class="overflow-hidden">
						<h4>Email</h4>
						<p class="lead">
							laikacafe0619@gmail.com
						</p>
					</div>
				</div>
				<div class="col-md-4">
					<i class="fa fa-map-marker"></i>
					<div class="overflow-hidden">
						<h4>Địa chỉ</h4>
						<p class="lead">
							17, Hàng Cót, Hoàn Kiếm, Hà Nội
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Contact info -->
	
	<!-- Start Footer -->
	<footer class="footer-area bg-f">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6">
					<h3>Về chúng tôi</h3>
					<p>LAIKA luôn hướng tới đáp ứng tốt nhất mong muốn của khách hàng về một quán cà phê. Vì vậy, đội ngũ LAIKA luôn làm việc hết mình, có trách nhiệm và thực sự đặt "cái tâm" của mình vào từng ly trà, cốc cà phê hay thâm chí là một cái bánh với một thái độ phục vụ chuyên nghiệp.</p>
				</div>
				<div class="col-lg-3 col-md-6">
					<h3>Các cơ sở của LAIKA</h3>
					<p>Số 64, Đường Láng, Đống Đa</p>
					<p>Số 52, Lĩnh Nam, Hoàng Mai</p>
					<p>Số 229, Phố Huế, Hai Bà Trưng</p>
					<p>Số 411, Hạ Long, Quảng Ninh</p>
					<p>Số 102, Quang Trung, Tuyên Quang</p>
					<p>Số 112, Lê Hồng Phong, Vinh</p>
				</div>
				<div class="col-lg-3 col-md-6">
					<h3>Thời gian mở cửa</h3>
					<p><span class="text-color">Thứ 2 - Thứ 7: </span>9h00 - 24h00</p>
					<p><span class="text-color">Chủ nhật: </span>7h00 - 24h00</p>
				</div>
				
				<div class="col-lg-3 col-md-6">
					<h3>Thông tin liên hệ</h3>
					<!-- <ul class="list-inline f-social"> -->
						<p class="list-inline-item"><a href="https://www.facebook.com/laikacafevn"><i class="fa fa-facebook" aria-hidden="true"></i> facebook.com/laikacafevn</a></p>
						
						<p class="list-inline-item"><a href="https://www.instagram.com/laikacafe.vn/?fbclid=IwAR09UZf4jzcC3BHuDLz9GumIVtB5JiXTs_QTnKKi7_2HjoV1yFB8EadRnJI"><i class="fa fa-instagram" aria-hidden="true"></i> instagram.com/laikacafe.vn</a></p>
					<!-- </ul> -->
				</div>
			</div>
		</div>
		
		<div class="copyright">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<p class="company-name">Copyright. &copy; 2020 <a href="trang_chu.php">LAIKA Café</a> Design By : 
					<a href="nhan_vien.php">Đội ngũ LAIKA</a></p>
					</div>
				</div>
			</div>
		</div>
		
	</footer>
	<!-- End Footer -->
	
	<a href="#" id="back-to-top" title="Back to top" style="display: none;">&uarr;</a>

	<!-- ALL JS FILES -->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
	<script src="js/jquery.superslides.min.js"></script>
	<script src="js/images-loded.min.js"></script>
	<script src="js/isotope.min.js"></script>
	<script src="js/baguetteBox.min.js"></script>
	<script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <script src="js/custom.js"></script>
</body>
</html>