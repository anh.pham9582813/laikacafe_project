<!DOCTYPE html>
<html lang="en"><!-- Basic -->
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">   
   
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
 
     <!-- Site Metas -->
    <title>Laika Cafe | Liên hệ</title>  
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="images/laikalogo.png" type="image/png">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">    
	<!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css">    
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

  

</head>

<body>
	<!-- Start header -->
	<header class="top-navbar">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="container">
				<a class="navbar-brand" href="trang_chu.php">
					<img src="images/abc.png" alt="" height="65.px" width="145.px" />
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars-rs-food" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbars-rs-food">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item"><a class="nav-link" href="trang_chu.php">Trang chủ</a></li>
						<li class="nav-item"><a class="nav-link" href="menu.php">Menu</a></li>
						<li class="nav-item"><a class="nav-link" href="gioi_thieu.php">Về chúng tôi</a></li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="dropdown-a" data-toggle="dropdown">Trang khác</a>
							<div class="dropdown-menu" aria-labelledby="dropdown-a">
								<a class="dropdown-item" href="dat_hang.php">Đặt hàng</a>
								<a class="dropdown-item" href="nhan_vien.php">Đội ngũ LAIKA</a>
								<a class="dropdown-item" href="chi_nhanh.php">Chi nhánh</a>
								<a class="dropdown-item" href="tuyen_dung.php">Tuyển dụng</a>
							</div>
						</li>
						<li class="nav-item"><a class="nav-link" href="tin_tuc.php">Tin tức</a></li>
						<li class="nav-item active"><a class="nav-link" href="lien_he.php">Liên hệ</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<!-- End header -->
	
	<!-- Start All Pages -->
	<div class="all-page-title page-breadcrumb">
		<div class="container text-center">
			<div class="row">
				<div class="col-lg-12">
					<h1>Liên hệ với chúng tôi</h1>
				</div>
			</div>
		</div>
	</div>
	<!-- End All Pages -->
	<div><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.7234833504936!2d105.81685471424491!3d21.00371859401021!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ad5075c91fb5%3A0xf02478b70ea852eb!2sLaika%20Cafe!5e0!3m2!1svi!2s!4v1635699805075!5m2!1svi!2s" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe></div>
	<!-- Start Contact -->
	<!-- <div class="map-full"></div> -->
	<div class="contact-box">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="heading-title text-center">
						<h2>Phản hồi</h2>
						<p>Những nhận xét của các bạn về LAIKA là động lực giúp chúng tôi hoàn thiện hơn</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<form method="Post" action="lien_he_thuc_hien.php" enctype="multipart/form-data">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<input type="text" class="form-control" id="inputemail" name="txttennguoigui" placeholder="Tên của bạn" required data-error="Hãy nhập tên của bạn">
									<div class="help-block with-errors"></div>
								</div>                                 
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<input type="text" placeholder="Email của bạn" id="inputemail" class="form-control" name="txtemaillienhe" required data-error="Hãy nhập email của bạn">
									<div class="help-block with-errors"></div>
								</div> 
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<select class="custom-select d-block form-control" name="txtbophan" id="" required data-error="Bộ phận chăm sóc khách hàng">
									<option disabled selected>Hãy chọn bộ phận chăm sóc khách hàng*</option>
									<?php
                                            include("config.php");

                                            $sql = "SELECT * 
                                                    FROM tbl_nhan_vien
                                                    ";
                                            
                                            $nhan_vien= mysqli_query($ket_noi,$sql);

                                            while ($row = mysqli_fetch_array($nhan_vien)) 
                                            {                                            
                                                ;?>
                                                    <option value="<?php echo $row["bo_phan"];?>"><?php echo $row["bo_phan"];?> </option>
                                                <?php
                                            }
                                        ;?>
									</select>
									<div class="help-block with-errors"></div>
								</div> 
							</div>
							<div class="col-md-12">
								<div class="form-group"> 
									<textarea class="form-control" id="txtnoidung" name ="txtnoidung" placeholder="Phản hồi của bạn" rows="7" data-error="Hãy viết phản hồi của bạn về LAIKA" required></textarea>
									<div class="help-block with-errors"></div>
								</div>
								<div class="submit-button text-center">
									<input class="btn btn-common" id="submit" type="submit" name="btnsubmit" value="Gửi phản hồi" />
									<div id="msgSubmit" class="h3 text-center hidden"></div> 
									<div class="clearfix"></div> 
								</div>
							</div>
						</div>            
					</form>
				</div>
			</div>
		</div>
	</div>
<
	<!-- End Contact -->
	
	<!-- Start thông tin liên hệ -->
	<div class="contact-imfo-box">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<i class="fa fa-volume-control-phone"></i>
					<div class="overflow-hidden">
						<h4>Di động</h4>
						<p class="lead">
							+84 941-050-819
						</p>
					</div>
				</div>
				<div class="col-md-4">
					<i class="fa fa-envelope"></i>
					<div class="overflow-hidden">
						<h4>Email</h4>
						<p class="lead">
							laikacafe0619@gmail.com
						</p>
					</div>
				</div>
				<div class="col-md-4">
					<i class="fa fa-map-marker"></i>
					<div class="overflow-hidden">
						<h4>Địa chỉ</h4>
						<p class="lead">
							17, Hàng Cót, Hoàn Kiếm, Hà Nội
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Contact info -->
	
	<!-- Start Footer -->
	<footer class="footer-area bg-f">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6">
					<h3>Về chúng tôi</h3>
					<p>LAIKA luôn hướng tới đáp ứng tốt nhất mong muốn của khách hàng về một quán cà phê. Vì vậy, đội ngũ LAIKA luôn làm việc hết mình, có trách nhiệm và thực sự đặt "cái tâm" của mình vào từng ly trà, cốc cà phê hay thâm chí là một cái bánh với một thái độ phục vụ chuyên nghiệp.</p>
				</div>
				<div class="col-lg-3 col-md-6">
					<h3>Các cơ sở của LAIKA</h3>
					<p>Số 64, Đường Láng, Đống Đa</p>
					<p>Số 52, Lĩnh Nam, Hoàng Mai</p>
					<p>Số 229, Phố Huế, Hai Bà Trưng</p>
					<p>Số 411, Hạ Long, Quảng Ninh</p>
					<p>Số 102, Quang Trung, Tuyên Quang</p>
					<p>Số 112, Lê Hồng Phong, Vinh</p>
				</div>
				<div class="col-lg-3 col-md-6">
					<h3>Thời gian mở cửa</h3>
					<p><span class="text-color">Thứ 2 - Thứ 7: </span>9h00 - 24h00</p>
					<p><span class="text-color">Chủ nhật: </span>7h00 - 24h00</p>
				</div>
				
				<div class="col-lg-3 col-md-6">
					<h3>Thông tin liên hệ</h3>
					<!-- <ul class="list-inline f-social"> -->
						<p class="list-inline-item"><a href="https://www.facebook.com/laikacafevn"><i class="fa fa-facebook" aria-hidden="true"></i> facebook.com/laikacafevn</a></p>
						<p class="list-inline-item"><a href="https://www.instagram.com/laikacafe.vn/?fbclid=IwAR09UZf4jzcC3BHuDLz9GumIVtB5JiXTs_QTnKKi7_2HjoV1yFB8EadRnJI"><i class="fa fa-instagram" aria-hidden="true"></i> instagram.com/laikacafe.vn</a></p>
					<!-- </ul> -->
				</div>
			</div>
		</div>
		
		<div class="copyright">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<p class="company-name">Copyright. &copy; 2020 <a href="trang_chu.php">LAIKA Café</a> Design By : 
					<a href="nhan_vien.php">Đội ngũ LAIKA</a></p>
					</div>
				</div>
			</div>
		</div>
		
	</footer>
	<!-- End Footer -->
	
	<a href="#" id="back-to-top" title="Back to top" style="display: none;">&uarr;</a>

	<!-- ALL JS FILES -->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
	
	<script src="js/jquery.superslides.min.js"></script>
	<script src="js/images-loded.min.js"></script>
	<script src="js/isotope.min.js"></script>
	<script src="js/baguetteBox.min.js"></script>
	<script src="js/jquery.mapify.js"></script>
	<script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <script src="js/custom.js"></script>
	<script>
		$('.map-full').mapify({
			points: [
				{
					lat: 40.7143528,
					lng: -74.0059731,
					marker: true,
					title: 'Marker title',
					infoWindow: 'Yamifood Restaurant'
				}
			]
		});	
	</script>
</body>
</html>