<!DOCTYPE html>
<html lang="en"><!-- Basic -->
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">   
   
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
 
     <!-- Site Metas -->
    <title>Laika Cafe | Đội ngũ LAIKA</title>  
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="images/laikalogo.png" type="image/png">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">    
	<!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css">    
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

</head>

<body>
	<!-- Start header -->
	<header class="top-navbar">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="container">
				<a class="navbar-brand" href="trang_chu.php">
					<img src="images/abc.png" alt="" height="90.px" width="180.px" />
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars-rs-food" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbars-rs-food">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item"><a class="nav-link" href="trang_chu.php">Trang chủ</a></li>
						<li class="nav-item"><a class="nav-link" href="menu.php">Menu</a></li>
						<li class="nav-item"><a class="nav-link" href="gioi_thieu.php">Về chúng tôi</a></li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="dropdown-a" data-toggle="dropdown">Trang khác</a>
							<div class="dropdown-menu" aria-labelledby="dropdown-a">
								<a class="dropdown-item" href="dat_hang.php">Đặt hàng</a>
								<a class="dropdown-item active" href="nhan_vien.php">Đội ngũ LAIKA</a>
								<a class="dropdown-item" href="chi_nhanh.php">Chi nhánh</a>
								<a class="dropdown-item" href="tuyen_dung.php">Tuyển dụng</a>
							</div>
						</li>
						<li class="nav-item"><a class="nav-link" href="tin_tuc.php">Tin tức</a></li>
						<li class="nav-item"><a class="nav-link" href="lien_he.php">Liên hệ</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<!-- End header -->
	
	<!-- Start All Pages -->
	<div class="all-page-title page-breadcrumb">
		<div class="container text-center">
			<div class="row">
				<div class="col-lg-12">
					<h1>Đội ngũ LAIKA</h1>
				</div>
			</div>
		</div>
	</div>
	<!-- End All Pages -->
	
	
	
	<!-- Start Customer Reviews -->
	<div class="customer-reviews-box">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="heading-title text-center">
						<h2>ĐỘI NGŨ SÁNG LẬP</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8 mr-auto ml-auto text-center">
					<div id="reviews" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner mt-4">
							<div class="carousel-item text-center active">
								<div class="img-box p-1 border rounded-circle m-auto">
									<img class="d-block w-100 rounded-circle" src="images/THT.JPG" alt="">
								</div>
								<h5 class="mt-4 mb-0"><strong class="text-warning text-uppercase">Trần Hoài Trang</strong></h5>
								
								<p class="m-0 pt-3">Thành công không phải số tiền bạn kiếm được, mà là sự khác biệt bạn tạo ra cho cuộc sống của người khác.</p>
							</div>

							<div class="carousel-item text-center">
								<div class="img-box p-1 border rounded-circle m-auto">
									<img class="d-block w-100 rounded-circle" src="images/NTLA.JPG" alt="">
								</div>
								<h5 class="mt-4 mb-0"><strong class="text-warning text-uppercase">Nguyễn Thị Lan Anh</strong></h5>
								
								<p class="m-0 pt-3">Bạn không thể chỉ hỏi khách hàng xem họ muốn gì và rồi cố đem nó cho họ. Tới lúc bạn hoàn thiện nó, họ đã muốn thứ mới mẻ khác rồi.</p>
							</div>
							<div class="carousel-item text-center">
								<div class="img-box p-1 border rounded-circle m-auto">
									<img class="d-block w-100 rounded-circle" src="images/ĐTP.jpg" alt="">
								</div>
								<h5 class="mt-4 mb-0"><strong class="text-warning text-uppercase">Đinh Thu Phương</strong></h5>
								
								<p class="m-0 pt-3">Chúng tôi luôn học hỏi từ đối thủ, về mọi thứ có thể học hỏi được, nhưng chúng tôi không bao giờ đi sao chép. Với chúng tôi, sao chép có nghĩa là chết. Cạnh tranh cũng giống như chơi một ván cờ. Khi chúng ta thua, chúng ta có thể chơi lại một ván cờ khác. Cả hai người chơi đừng nên chiến đấu triệt hạ lẫn nhau.</p>
							</div>
							<div class="carousel-item text-center">
								<div class="img-box p-1 border rounded-circle m-auto">
									<img class="d-block w-100 rounded-circle" src="images/pma.jpg" alt="">
								</div>
								<h5 class="mt-4 mb-0"><strong class="text-warning text-uppercase">Phạm Thị Mai Anh</strong></h5>
								
								<p class="m-0 pt-3">Nếu bạn xây dựng được trải nghiệm tốt thì khách hàng sẽ kể cho nhau nghe về điều đó. Tin truyền miệng là thứ lan tỏa rất nhanh.</p>
							</div>
							<div class="carousel-item text-center">
								<div class="img-box p-1 border rounded-circle m-auto">
									<img class="d-block w-100 rounded-circle" src="images/TTHT.JPG" alt="">
								</div>
								<h5 class="mt-4 mb-0"><strong class="text-warning text-uppercase">Trịnh Thị Huyền Trang</strong></h5>
								
								<p class="m-0 pt-3">Những khách hàng không hài lòng sẽ là bài học tuyệt vời cho bạn.</p>
							</div>
						</div>

						<a class="carousel-control-prev" href="#reviews" role="button" data-slide="prev">
							<i class="fa fa-angle-left" aria-hidden="true"></i>
							<span class="sr-only">Previous</span>
						</a>
						<a class="carousel-control-next" href="#reviews" role="button" data-slide="next">
							<i class="fa fa-angle-right" aria-hidden="true"></i>
							<span class="sr-only">Next</span>
						</a>
                    </div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Customer Reviews -->
	
	<!-- Start Stuff -->
	<div class="stuff-box">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="heading-title text-center">
						<h2>NHÂN VIÊN XUẤT SẮC CỦA THÁNG</h2>
						
					</div>
				</div>
			</div>

			<div class="row">
			<?php
			include("config.php");
			$sql="SELECT * FROM tbl_nhan_vien ORDER BY id_nhan_vien DESC";
			$nhan_vien=mysqli_query($ket_noi,$sql);
			while($row =mysqli_fetch_array($nhan_vien))
			{
			;?>			
				<div class="col-md-4 col-sm-6">
					<div class="our-team">
						<img src="<?php echo $row['anh'] ? 'images/'.$row['anh'] : 'images/ĐTP.jpg' ;?>"< class="img-fluid" alt="Image" >
						<div class="team-content">
							<h3 class="title"><?php ECHO $row['ten_nhan_vien'] ;?></h3>
						</div>
					</div>
				</div>
				<?php
				} ;?>
			</div>
		</div>
	</div>	
	<!-- End Stuff -->

	<!-- Start thông tin liên hệ -->
	<div class="contact-imfo-box">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<i class="fa fa-volume-control-phone"></i>
					<div class="overflow-hidden">
						<h4>Di động</h4>
						<p class="lead">
							+84 941-050-819
						</p>
					</div>
				</div>
				<div class="col-md-4">
					<i class="fa fa-envelope"></i>
					<div class="overflow-hidden">
						<h4>Email</h4>
						<p class="lead">
							laikacafe0619@gmail.com
						</p>
					</div>
				</div>
				<div class="col-md-4">
					<i class="fa fa-map-marker"></i>
					<div class="overflow-hidden">
						<h4>Địa chỉ</h4>
						<p class="lead">
							17, Hàng Cót, Hoàn Kiếm, Hà Nội
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Contact info -->
	
	<!-- Start Footer -->
	<footer class="footer-area bg-f">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6">
					<h3>Về chúng tôi</h3>
					<p>LAIKA luôn hướng tới đáp ứng tốt nhất mong muốn của khách hàng về một quán cà phê. Vì vậy, đội ngũ LAIKA luôn làm việc hết mình, có trách nhiệm và thực sự đặt "cái tâm" của mình vào từng ly trà, cốc cà phê hay thâm chí là một cái bánh với một thái độ phục vụ chuyên nghiệp.</p>
				</div>
				<div class="col-lg-3 col-md-6">
					<h3>Các cơ sở của LAIKA</h3>
					<p>Số 64, Đường Láng, Đống Đa</p>
					<p>Số 52, Lĩnh Nam, Hoàng Mai</p>
					<p>Số 229, Phố Huế, Hai Bà Trưng</p>
					<p>Số 411, Hạ Long, Quảng Ninh</p>
					<p>Số 102, Quang Trung, Tuyên Quang</p>
					<p>Số 112, Lê Hồng Phong, Vinh</p>
				</div>
				<div class="col-lg-3 col-md-6">
					<h3>Thời gian mở cửa</h3>
					<p><span class="text-color">Thứ 2 - Thứ 7: </span>9h00 - 24h00</p>
					<p><span class="text-color">Chủ nhật: </span>7h00 - 24h00</p>
				</div>
				
				<div class="col-lg-3 col-md-6">
					<h3>Thông tin liên hệ</h3>
					<!-- <ul class="list-inline f-social"> -->
						<p class="list-inline-item"><a href="https://www.facebook.com/laikacafevn"><i class="fa fa-facebook" aria-hidden="true"></i> facebook.com/laikacafevn</a></p>
						
						<p class="list-inline-item"><a href="https://www.instagram.com/laikacafe.vn/"><i class="fa fa-instagram" aria-hidden="true"></i> instagram.com/laikacafe.vn</a></p>
					<!-- </ul> -->
				</div>
			</div>
		</div>
		
		<div class="copyright">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<p class="company-name">Copyright. &copy; 2020 <a href="trang_chu.php">LAIKA Café</a> Design By : 
					<a href="nhan_vien.php">Đội ngũ LAIKA</a></p>
					</div>
				</div>
			</div>
		</div>
		
	</footer>
	<!-- End Footer -->
	
	<a href="#" id="back-to-top" title="Back to top" style="display: none;">&uarr;</a>

	<!-- ALL JS FILES -->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
	<script src="js/jquery.superslides.min.js"></script>
	<script src="js/images-loded.min.js"></script>
	<script src="js/isotope.min.js"></script>
	<script src="js/baguetteBox.min.js"></script>
	<script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <script src="js/custom.js"></script>
</body>
</html>