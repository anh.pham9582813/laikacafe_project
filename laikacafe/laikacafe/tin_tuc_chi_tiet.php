<?php 
    session_start();
    include('config.php');
    $id_tin_tuc = $_GET['id'];
    
    $sql1= "SELECT * FROM tbl_tin_tuc 
          WHERE `tbl_tin_tuc`.`id_tin_tuc` = '".$id_tin_tuc."'";
    
    $truy_cap= mysqli_fetch_array(mysqli_query($ket_noi,$sql1));
    
    $_SESSION['luotdoc']= $truy_cap['so_lan_doc'];
    
    if(isset($_SESSION['luotdoc']))
    
    $_SESSION['luotdoc']+=1;
    
    $sql2 = "UPDATE `tbl_tin_tuc` SET so_lan_doc = '".$_SESSION['luotdoc']."' WHERE tbl_tin_tuc.id_tin_tuc = '".$id_tin_tuc."'" ;  
    
    $lan_doc = mysqli_query($ket_noi, $sql2);
    
    mysqli_close($ket_noi);
?>
<!DOCTYPE html>
<html lang="en"><!-- Basic -->
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">   
   
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
 
     <!-- Site Metas -->
    <title>Laika Cafe | Tin tức chi tiết</title>  
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="images/laikalogo.png" type="image/png">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">    
	<!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css">    
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
	<!-- Start header -->
	<header class="top-navbar">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="container">
				<a class="navbar-brand" href="trang_chu.php">
					<img src="images/abc.png" alt="" height="65.px" width="145.px" />
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars-rs-food" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbars-rs-food">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item"><a class="nav-link" href="trang_chu.php">Trang chủ</a></li>
						<li class="nav-item"><a class="nav-link" href="menu.php">Menu</a></li>
						<li class="nav-item"><a class="nav-link" href="gioi_thieu.php">Về chúng tôi</a></li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="dropdown-a" data-toggle="dropdown">Trang khác</a>
							<div class="dropdown-menu" aria-labelledby="dropdown-a">
								<a class="dropdown-item" href="dat_hang.php">Đặt hàng</a>
								<a class="dropdown-item" href="nhan_vien.php">Nhân viên</a>
								<a class="dropdown-item" href="chi_nhanh.php">Chi nhánh</a>
							</div>
						</li>
						<li class="nav-item"><a class="nav-link" href="tin_tuc.php">Tin tức</a></li>
						<li class="nav-item"><a class="nav-link" href="lien_he.php">Liên hệ</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<!-- End header -->
	
	<!-- Start All Pages -->
	<div class="all-page-title page-breadcrumb">
		<div class="container text-center">
			<div class="row">
				<div class="col-lg-12">
					<h1>Bảng tin nhà LAIKA</h1>
				</div>
			</div>
		</div>
	</div>
	<!-- End All Pages -->

	<!-- Start blog details -->
	<div class="blog-box">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="heading-title text-center">
						<h2>Bài viết mới</h2>
						<p>Chi tiết bài viết</p>
					</div>
				</div>
			</div>


			<?php  
				          include("config.php");

				          $id_tin_tuc = $_GET['id'];
				          $sql = "
				                    SELECT * 
				                    FROM tbl_tin_tuc
				                    WHERE `tbl_tin_tuc`.`id_tin_tuc` = '".$id_tin_tuc."'
				          ";
				          $noi_dung_tin_tuc = mysqli_query($ket_noi, $sql);

				          $row = mysqli_fetch_array($noi_dung_tin_tuc)
		    ;?>

			<div class="row">
				<div class="col-xl-8 col-lg-8 col-12">
					<div class="blog-inner-details-page">
						<div class="blog-inner-box">
							<div class="side-blog-img">
								<img class="img-fluid" src="<?php echo $row['anh_minh_hoa'] ? 'images/'.$row['anh_minh_hoa'] : 'images/abc.png' ;?>" alt="">							
								<div class="date-blog-up">
									Laika Cafe
								</div>
							</div>
							<div class="inner-blog-detail details-page">
								<h1><b><?php echo $row["tieu_de"]; ?></b></h1>
			
								<blockquote>
									<p><?php echo $row["mo_ta"]; ?></p>
								</blockquote>
								<p><?php echo $row["noi_dung"]; ?></p>
							</div>
						</div>
					</div>
				</div>
			
				<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-12 blog-sidebar">
					<div class="right-side-blog">


						<h3>Bài viết gần đây</h3>
						<div class="post-box-blog">
							<div class="recent-post-box">


								<?php  
					                    include("config.php");

					                    $sql =  "
					                              SELECT *
					                              FROM tbl_tin_tuc 
					                              ORDER BY id_tin_tuc DESC
					                              LIMIT 4
					                            ";

					                    $noi_dung_tin_tuc =  mysqli_query($ket_noi, $sql);

					                    while ($row = mysqli_fetch_array($noi_dung_tin_tuc)) 
					                    {
		                		;?>

										<div class="recent-box-blog">
											<div class="recent-img">
												<img class="img-fluid" src="<?php echo $row['anh_minh_hoa'] ? 'images/'.$row['anh_minh_hoa'] : 'images/abc.png' ;?>" alt="" height="80px", width="80px">
											</div>
											<div class="recent-info">
												<ul>
													<li><i class="zmdi zmdi-account"></i>Posted By Admin <span></span></li>
													<li>|</li>
													<li><i class="zmdi zmdi-time"></i>Time : <span><?php echo date("H:i d/m/Y", strtotime($row["ngay_dang"]));?></span></li>
												</ul>
												<h4><a href="tin_tuc_chi_tiet.php?id=<?php echo $row["id_tin_tuc"]; ?>"><?php echo $row["tieu_de"]; ?></a></h4>
											</div>
										</div>

									<?php     
					                    }
					                 ;?>

							</div>
						</div>

						<h3>Phân loại</h3>
						<div class="blog-categories">
							<ul>
								<li><a href="chi_nhanh.php"><span>Chi nhánh</span></a></li>
								<li><a href="menu.php"><span>Đồ uống</span></a></li>
								<li><a href="tin_tuc.php"><span>Check-in</span></a></li>
							</ul>
						</div>

						<h3>Thẻ gần đây</h3>
						<div class="blog-tag-box">
							<ul class="list-inline tag-list">
								<li class="list-inline-item"><a href="chi_nhanh.php">Cửa hàng</a></li>
								<li class="list-inline-item"><a href="menu.php">Đồ uống</a></li>
								<li class="list-inline-item"><a href="tin_tuc.php">Check-in</a></li>
								<li class="list-inline-item"><a href="menu.php">Trà</a></li>
								<li class="list-inline-item"><a href="menu.php">Coffee</a></li>
							</ul>
						</div>
					</div>
				</div>
			
			</div>
		</div>
	</div>
	<!-- End details -->
	
	
	<!-- Start thông tin liên hệ -->
	<div class="contact-imfo-box">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<i class="fa fa-volume-control-phone"></i>
					<div class="overflow-hidden">
						<h4>Di động</h4>
						<p class="lead">
							+84 941-050-819
						</p>
					</div>
				</div>
				<div class="col-md-4">
					<i class="fa fa-envelope"></i>
					<div class="overflow-hidden">
						<h4>Email</h4>
						<p class="lead">
							laikacafe0619@gmail.com
						</p>
					</div>
				</div>
				<div class="col-md-4">
					<i class="fa fa-map-marker"></i>
					<div class="overflow-hidden">
						<h4>Địa chỉ</h4>
						<p class="lead">
							17, Hàng Cót, Hoàn Kiếm, Hà Nội
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Contact info -->
	
	<!-- Start Footer -->
	<footer class="footer-area bg-f">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6">
					<h3>Về chúng tôi</h3>
					<p>LAIKA luôn hướng tới đáp ứng tốt nhất mong muốn của khách hàng về một quán cà phê. Vì vậy, đội ngũ LAIKA luôn làm việc hết mình, có trách nhiệm và thực sự đặt "cái tâm" của mình vào từng ly trà, cốc cà phê hay thâm chí là một cái bánh với một thái độ phục vụ chuyên nghiệp.</p>
				</div>
				<div class="col-lg-3 col-md-6">
					<h3>Các cơ sở của LAIKA</h3>
					<p>Số 64, Đường Láng, Đống Đa</p>
					<p>Số 52, Lĩnh Nam, Hoàng Mai</p>
					<p>Số 229, Phố Huế, Hai Bà Trưng</p>
					<p>Số 411, Hạ Long, Quảng Ninh</p>
					<p>Số 102, Quang Trung, Tuyên Quang</p>
					<p>Số 112, Lê Hồng Phong, Vinh</p>
				</div>
				<div class="col-lg-3 col-md-6">
					<h3>Thời gian mở cửa</h3>
					<p><span class="text-color">Thứ 2 - Thứ 7: </span>9h00 - 24h00</p>
					<p><span class="text-color">Chủ nhật: </span>7h00 - 24h00</p>
				</div>
				
				<div class="col-lg-3 col-md-6">
					<h3>Thông tin liên hệ</h3>
					<!-- <ul class="list-inline f-social"> -->
						<p class="list-inline-item"><a href="https://www.facebook.com/laikacafevn"><i class="fa fa-facebook" aria-hidden="true"></i> facebook.com/laikacafevn</a></p>
						
						<p class="list-inline-item"><a href="https://www.instagram.com/laikacafe.vn/?fbclid=IwAR09UZf4jzcC3BHuDLz9GumIVtB5JiXTs_QTnKKi7_2HjoV1yFB8EadRnJI"><i class="fa fa-instagram" aria-hidden="true"></i> instagram.com/laikacafe.vn</a></p>
					<!-- </ul> -->
				</div>
			</div>
		</div>
		
		<div class="copyright">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<p class="company-name">Copyright. &copy; 2020 <a href="trang_chu.php">LAIKA Café</a> Design By : 
					<a href="nhan_vien.php">Đội ngũ LAIKA</a></p>
					</div>
				</div>
			</div>
		</div>
		
	</footer>
	<!-- End Footer -->
	
	<a href="#" id="back-to-top" title="Back to top" style="display: none;">&uarr;</a>

	<!-- ALL JS FILES -->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
	
	<script src="js/jquery.superslides.min.js"></script>
	<script src="js/images-loded.min.js"></script>
	<script src="js/isotope.min.js"></script>
	<script src="js/baguetteBox.min.js"></script>
	<script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <script src="js/custom.js"></script>
</body>
</html>